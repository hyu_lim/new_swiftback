//
//  PageContentViewController.h
//  SwiftBack
//
//  Created by Hengyu Lim on 9/4/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *titleLabel2;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)onClickBackButton:(id)sender;

@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *titleText2;
@property NSString *imageFile;



@end
