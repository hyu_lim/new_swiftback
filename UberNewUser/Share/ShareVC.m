//
//  ShareVC.m
//  TaxiNow
//
//  Created by My Mac on 6/23/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "ShareVC.h"
#import "Social/Social.h"
#import "ContactUsVC.h"
#import "SWRevealViewController.h"


@interface ShareVC ()

@end

@implementation ShareVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self layoutSetup];
    [self setLocalizedStrings];
}

- (void)setLocalizedStrings {
    [self.lblTitle setText:NSLocalizedString(@"SHARE", nil)];
    [self.lblText1 setText:NSLocalizedString(@"SOMETHING", nil)];
    [self.lblText2 setText:NSLocalizedString(@"WORTH_SHARING", nil)];
}

- (void)layoutSetup {
    //rectangle background Rect
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //rectangle background view
    _backgroundView.layer.cornerRadius = 10;
    _backgroundView.layer.borderWidth = 1;
    _backgroundView.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundView.layer.shadowRadius = 5.0;
    _backgroundView.layer.shadowOpacity = 0.4;
    
    // btnFacebook
    _btnFacebook.layer.cornerRadius = 5;
    _btnFacebook.layer.borderWidth = 0.1;
    _btnFacebook.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnWhatsapp
    _btnWhatsapp.layer.cornerRadius = 5;
    _btnWhatsapp.layer.borderWidth = 0.1;
    _btnWhatsapp.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnMail
    _btnMail.layer.cornerRadius = 5;
    _btnMail.layer.borderWidth = 0.1;
    _btnMail.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
}

- (IBAction)onClickFacebook:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:NSLocalizedString(@"FACEBOOK_SHARE_TEXT", nil)];
        [controller addURL:[NSURL URLWithString:@"http://www.swiftback.com"]];
        [controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image-01.jpg"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_NOT_INSTALLED_MSG_TITLE", nil) message:NSLocalizedString(@"FACEBOOK_NOT_INSTALLED_MSG", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)onClickTwitter:(id)sender {
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:NSLocalizedString(@"TWITTER_SHARE_TEXT", nil)];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}

- (IBAction)onClickWhatsapp:(id)sender {
    
    NSString *message = NSLocalizedString(@"WHATSAPP_SHARE_TEXT", nil);
    NSString *url = [NSString stringWithFormat:@"whatsapp://send?text=%@",message];
    NSURL *whatsappURL = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    if([[UIApplication sharedApplication] canOpenURL:whatsappURL]) {
        [[UIApplication sharedApplication] openURL:whatsappURL];
    } else {
       
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"WHATSAPP_ALERT_TITLE", nil) message:NSLocalizedString(@"WHATSAPP_ERROR_TEXT", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)onClickMsg:(id)sender {
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:NSLocalizedString(@"MAIL_SUBJECT_TEXT", nil)];
    [mailComposer setMessageBody:NSLocalizedString(@"MAIL_SHARE_TEXT", nil) isHTML:YES];
    //[mailComposer setMessageBody:@"Testing message for the test mail" isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:nil];
}

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.lblNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
