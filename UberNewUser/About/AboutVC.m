//
//  AboutVC.m
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "AboutVC.h"
#import "SWRevealViewController.h"
#import "UIView+Utils.h"
#import "ExpandingCellTableViewCell.h"

@interface AboutVC ()
{
    NSString *strForHtml;
}

@end

@implementation AboutVC {
    UIView *backgroundOverlayView;
    UIView* loadingView;
}


#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self layoutSetup];
    [self setLocalizedStrings];
    
    self.navigationController.navigationBarHidden=YES;
    self.btnNavigation.titleLabel.font=[UberStyleGuide fontRegular];
    //[super setNavBarTitle:TITLE_ABOUT];
    //[super setBackBarItem];
    
    for(NSMutableDictionary *dict in self.arrInformation)
    {
        if([[dict valueForKey:@"title"] isEqualToString:@"About Us"])
        {
            strForHtml=[dict valueForKey:@"content"];
        }
    }
    
     [self.webView loadHTMLString:strForHtml baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
   // [self.imgSwift applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    
    //WEBVIEW
    
    NSURL *websiteUrl = [NSURL URLWithString:ABOUT_URL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webViewAbout loadRequest:urlRequest];
    
    
    //activity indicator for web view
    
    loadingView = [[UIView alloc]initWithFrame:CGRectMake(120, 200, 80, 80)];
    loadingView.backgroundColor = [UIColor colorWithRed:65/255.0f green:183/255.0f blue:154/255.0f alpha:0.9f];
    loadingView.layer.cornerRadius = 5;
    
    
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
    [activityView startAnimating];
    activityView.tag = 100;
    [loadingView addSubview:activityView];
    
    UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
    lblLoading.text = NSLocalizedString(@"LOADING", nil);
    lblLoading.textColor = [UIColor whiteColor];
    lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:12];
    lblLoading.textAlignment = NSTextAlignmentCenter;
    [loadingView addSubview:lblLoading];
    
    [self.view addSubview:backgroundOverlayView];
    [self.view addSubview:loadingView];
    [self.view bringSubviewToFront:loadingView];
    _webViewAbout.delegate = self;
}

- (void)setLocalizedStrings {
    self.lblTitle.text=NSLocalizedString(@"ABOUT", nil);
    [self.btnTutorial setTitle:NSLocalizedString(@"WALKTHROUGH", nil) forState:UIControlStateNormal];
}

- (void)layoutSetup {
    //Walkthrough button
    //register button
    _btnTutorial.layer.cornerRadius = 5;
    _btnTutorial.layer.borderWidth = 1;
    _btnTutorial.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //rectangle background Rect
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
    
    //blackOverlayView behind of loading sign
    backgroundOverlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 700)];
    backgroundOverlayView.layer.backgroundColor = [UIColor blackColor].CGColor;
    backgroundOverlayView.layer.opacity = 0.5;

}

- (void)webViewDidFinishLoad:(UIWebView *)webViewAbout {
    [backgroundOverlayView setHidden:YES];
    [loadingView setHidden:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webViewAbout {
    [backgroundOverlayView setHidden:NO];
    [loadingView setHidden:NO];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.lblNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    }
}
#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)onClickTutorial:(id)sender {
}
@end
