//
//  TutorialVC.h
//  SwiftBack
//
//  Created by My Mac on 7/10/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "BaseVC.h"

@interface TutorialVC : BaseVC <UIGestureRecognizerDelegate>
{

}

@property (weak, nonatomic) IBOutlet UIImageView *img;

@end
