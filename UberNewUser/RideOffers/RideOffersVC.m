//
//  RideOffersVC.m
//  SwiftBack
//
//  Created by Elluminati on 22/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "RideOffersVC.h"
#import "SWRevealViewController.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "AdvancePickupVC.h"
#import "Social/Social.h"


@interface RideOffersVC ()
{
    UITableViewController *tableViewController;
    NSUInteger page,totalPagesNearest,totalPagesEarliest,NearestPage,EarliestPage;
    NSMutableArray *arrNearest,*arrEarliest;
    BOOL is_first;
    NSString *currencySign;
    NSString *currentCountry;
}
@end

@implementation RideOffersVC{;
    UIView* loadingView;
}

- (void)viewDidLoad {
    
	[super viewDidLoad];
    [self customSetup];
    [self layoutSetup];
    [self setLocalizedStrings];
    
	arrOffers = [[NSMutableArray alloc]init];
    arrNearest = [[NSMutableArray alloc]init];
    arrEarliest = [[NSMutableArray alloc]init];
	
    NearestPage=1;
    EarliestPage=1;
    totalPagesNearest=1;
    totalPagesEarliest=1;
    is_first=YES;
    
    //Pull to refresh table view conifguration
    tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = tableForOffers;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor darkGrayColor];
    [self.refreshControl addTarget:self action:@selector(getOffers) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;
    
    // for reload more data
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:40.0f tableView:tableForOffers withClient:self];
    
    //Tap Gesture Recognizer for FB Sharing View
    UITapGestureRecognizer *tapGestureRecognizerForFBShareing = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOptionToShare:)];
    [self.viewForFBSharing addGestureRecognizer:tapGestureRecognizerForFBShareing];
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
}

- (void)setLocalizedStrings {
    [self.lblNavTitle setText:NSLocalizedString(@"RIDE_OFFERS", nil)];
    [self.lblText1 setText:NSLocalizedString(@"COULD_NOT_GET_A_RIDE", nil)];
    [self.lblText2 setText:NSLocalizedString(@"SHARE_SWIFTBACK_TEXT", nil)];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getOffers:NearestPage];
	[self confirmOfferCount];
}

-(void)confirmOfferCount
{
	NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
	[dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
	[dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
	
	AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
	[helper getDataFromPath:FILE_CONFIRM_OFFER_COUNT withParamData:dictParam withBlock:^(id response, NSError *error) {
		if (response)
		{
			if ([[response objectForKey:@"success"] boolValue])
			{

                
				if ([[response objectForKey:@"offer_count"] integerValue ]== 0)
				{
					[confirmBtn setTitle:@"" forState:UIControlStateNormal];
					[confirmBtn setBackgroundImage:[UIImage imageNamed:@"icon_tick_white"] forState:UIControlStateNormal];
					[confirmBtn setBackgroundColor:[UIColor clearColor]];
				}
				else
				{
					[confirmBtn setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];

					[confirmBtn setTitle:[NSString stringWithFormat:@"%ld",(long)[[response objectForKey:@"offer_count"] integerValue]] forState:UIControlStateNormal];
					[confirmBtn setBackgroundColor:[UIColor redColor]];
					
				}
			}
		}
	}];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)customSetup
{
	SWRevealViewController *revealViewController = self.revealViewController;
	if ( revealViewController )
	{
		[menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
		//Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
	}
}

- (void)layoutSetup {
    
    //confirmBtn
    confirmBtn.layer.cornerRadius = 5;
    confirmBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    confirmBtn.layer.borderWidth = 1.5f;
    
    //NYSegmentedControl
    
    self.customSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"NEAREST", nil), NSLocalizedString(@"EARLIEST", nil)]];
    self.customSegmentedControl.frame = CGRectMake(10, 60, self.view.frame.size.width - 20, 35);
    
    [self.customSegmentedControl addTarget:self action:@selector(segmentSelected) forControlEvents:UIControlEventValueChanged];
    
    self.customSegmentedControl.titleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleFont = [UIFont systemFontOfSize:12.0f];
    
    self.customSegmentedControl.backgroundColor = [UIColor colorWithRed:37.f/255.f green:131.f/255.f blue:111.f/255.f alpha:1.0f];
    
    self.customSegmentedControl.borderWidth = 0.0f;
    
    self.customSegmentedControl.segmentIndicatorBackgroundColor = [UIColor colorWithRed:59.f/255.f green:177.f/255.f blue:156.f/255.f alpha:1.0f];
    self.customSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    self.customSegmentedControl.segmentIndicatorInset = 2.0f;
    self.customSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    
    self.customSegmentedControl.cornerRadius = CGRectGetHeight(self.customSegmentedControl.frame) / 2.0f;
    
    [self.view addSubview:self.customSegmentedControl];
    
    self.customSegmentedControl.contentMode = UIViewContentModeScaleAspectFill;
    
    self.customSegmentedControl.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin);
    
    self.customSegmentedControl.clipsToBounds = YES;
    
    //ViewForFBSharing
    self.viewForFBSharing.layer.cornerRadius = 10;
    self.viewForFBSharing.clipsToBounds = YES;
}

 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
	 
	 if ([[segue identifier] isEqualToString:SEGUE_TO_ADVANCE_PICKUP]) {
		 AdvancePickupVC *advance = [segue destinationViewController];
		 [advance setIsOffer:@"1"];
		 [advance setDictRequest:sender];
	 }
 }

#pragma mark
#pragma mark - TableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [arrOffers count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	RideNextCell *cellZero = [tableView dequeueReusableCellWithIdentifier:@"CellRideNext"];
	
    NSDictionary *dictRequest = [arrOffers objectAtIndex:indexPath.row];
	
	UIView *viewForCellOne = [cellZero viewWithTag:904];
	viewForCellOne.layer.cornerRadius = 10;
	viewForCellOne.clipsToBounds = YES;
	
	//Adjust horizontal and vertical lines size 
	UILabel *horizontalLine = [cellZero viewWithTag:908];
	UILabel *verticalLine1 = [cellZero viewWithTag:909];
	UILabel *verticalLine2 = [cellZero viewWithTag:910];
	
	horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
	
	verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
	
	verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);
	
	NSDictionary *dictWalker = [dictRequest objectForKey:@"walker"];
	
	
	[cellZero.profileImgView downloadFromURL:[dictWalker valueForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC3"]];
	[cellZero.lblName setText:[dictWalker valueForKey:@"name"]];
    [cellZero.lblNumberOfRate setText:[NSString stringWithFormat:@"(%@)",[dictWalker valueForKey:@"num_rating"]]];
	
	[cellZero.profileImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
	[cellZero.paymentImgView setImage:[UIImage imageNamed:@"icon_cash"]];

	[cellZero.ratingView setUserInteractionEnabled:NO];
	
	RBRatings ratings = (float)([[dictWalker objectForKey:@"rating"] floatValue]*2);
	[cellZero.ratingView setRatings:ratings];
	

	[cellZero.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
	[cellZero.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];
    
    
	[cellZero.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:@"total"]]];
	[cellZero.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
	
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	[formatter setDateFormat:@"yyyy-MM-dd"];//2013-07-15:10:00:00
	
	NSDate *date = [formatter dateFromString:[dictRequest objectForKey:@"request_date"]];
	
	[formatter setDateFormat:@"dd MMM"];
	[cellZero.lblDepartureDate setText:[formatter stringFromDate:date]];
	[cellZero.lblDepartureTime setText:[dictRequest objectForKey:@"request_time"]];
	
	return cellZero;
	
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.f;
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView.rowHeight;
}*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//    return 160.0f;
	return 105.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	NSDictionary *dictSelected = [arrOffers objectAtIndex:indexPath.row];
	
	[self performSegueWithIdentifier:SEGUE_TO_ADVANCE_PICKUP sender:dictSelected];
}

-(void)getOffers
{
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        arrNearest=[[NSMutableArray alloc] init];
        NearestPage=1;
        [self getOffers:NearestPage];
    }
    else
    {
        arrEarliest=[[NSMutableArray alloc] init];
        EarliestPage=1;
        [self getOffers:EarliestPage];
    }
    
}

- (void)reloadData {
    //Reload table data
    [tableForOffers reloadData];
    
    //End the refreshing
    if(self.refreshControl) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update : %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrDictionary = [NSDictionary dictionaryWithObject:[UIColor darkGrayColor] forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        [self.refreshControl endRefreshing];
    }
}

- (IBAction)onClickCloseShareView:(id)sender {
    
    [tableForOffers setFrame:CGRectMake(tableForOffers.frame.origin.x, tableForOffers.frame.origin.y, tableForOffers.frame.size.width, self.view.frame.size.height - 115)];
    
    [self.viewForFBSharing setHidden:YES];
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.5;
    
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition.type = kCATransitionFade;
    
    transition.delegate = self;
    
    [self.viewForFBSharing.layer addAnimation:transition forKey:nil];
}

- (IBAction)onClickConfirmOffers:(id)sender {
	
	[self performSegueWithIdentifier:SEGUE_TO_CONFIRM_OFFERS sender:nil];
	
}

- (IBAction)onClickSegment:(id)sender
{
    [self segmentSelected];
}

- (void)segmentSelected
{
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        arrOffers = [arrNearest mutableCopy];
        if(NearestPage>=totalPagesNearest)
        {
            [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
        }
        else
        {
            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
        }

    }
    else
    {
        if(arrEarliest.count==0)
        {
            if(EarliestPage==1)
            {
                is_first=YES;
                [self getOffers:EarliestPage];
            }
        }
        else
        {
            arrOffers = [arrEarliest mutableCopy];
            if(EarliestPage>=totalPagesNearest)
            {
                [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
            }
            else
            {
                [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
            }
        }
    }
    if ([arrOffers count] > 0)
    {
        [tableForOffers setHidden:NO];
        [no_items setHidden:YES];
        [tableForOffers reloadData];
    }
    else
    {
        [tableForOffers setHidden:YES];
        [no_items setHidden:NO];
    }
    [pullToRefreshManager_ relocatePullToRefreshView];
}

- (void)showOptionToShare:(UITapGestureRecognizer *)tapGestureRecognizer {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"SHARE_WITH", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"FACEBOOK_", nil), NSLocalizedString(@"WHATSAPP_", nil), NSLocalizedString(@"EMAIL_", nil), nil];
    actionSheet.tag = 2000;
    [actionSheet showInView: self.view];
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self shareOnFacebook];
            break;
        case 1:
            [self shareOnWhatsApp];
            break;
        case 2:
            [self shareWithEmail];
            break;
        default:
            break;
    }
}

- (void)shareOnFacebook {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:NSLocalizedString(@"FACEBOOK_SHARE_TEXT", nil)];
        [controller addURL:[NSURL URLWithString:@"http://www.swiftback.com"]];
        [controller addImage:[UIImage imageNamed:@"socialsharing-facebook-image-01.jpg"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR_TITLE", nil) message:NSLocalizedString(@"FACEBOOK_ERROR_TEXT", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)shareOnWhatsApp {
    
    NSString *message = NSLocalizedString(@"WHATSAPP_SHARE_TEXT", nil);
    NSString *url = [NSString stringWithFormat:@"whatsapp://send?text=%@",message];
    NSURL *whatsappURL = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    if([[UIApplication sharedApplication] canOpenURL:whatsappURL]) {
        [[UIApplication sharedApplication] openURL:whatsappURL];
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"WHATSAPP_ALERT_TITLE", nil) message:NSLocalizedString(@"WHATSAPP_ERROR_TEXT", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)shareWithEmail {
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:NSLocalizedString(@"CHECK_THIS_OUT", nil)];
    [mailComposer setMessageBody:NSLocalizedString(@"RIDE_SHARING_APP_WITH_LINK", nil) isHTML:YES];
    //[mailComposer setMessageBody:@"Testing message for the test mail" isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:NULL];
}

#pragma mark - 
#pragma mark - ReloadTable to bottom
- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager
{
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
}

- (void)loadTable
{
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        NearestPage++;
        page=NearestPage;
        if(page<=totalPagesNearest)
        {
            [self getOffers:page];
        }
    }
    else
    {
        EarliestPage++;
        page=EarliestPage;
        if(page<=totalPagesEarliest)
        {
            [self getOffers:page];
        }
    }
    [pullToRefreshManager_ tableViewReloadFinished];
    
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [pullToRefreshManager_ relocatePullToRefreshView];
}

-(void)getOffers:(NSInteger)numberOfPage
{
    if ([APPDELEGATE connected])
    {
        if(is_first==YES)
        {
            [APPDELEGATE showLoadingWithTitle:@"Loading"];
            is_first=NO;
        }
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:strForCurLatitude forKey:PARAM_LATITUDE];
        [dictParam setObject:strForCurLongitude forKey:PARAM_LONGITUDE];
        [dictParam setObject:[NSString stringWithFormat:@"%ld",(long)numberOfPage] forKey:PARAM_PAGE];
        
        if (self.customSegmentedControl.selectedSegmentIndex == 0) {
            
            [dictParam setObject:@"1" forKey:@"nearest"];
        }
        else{
            [dictParam setObject:@"0" forKey:@"nearest"];
        }
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_DRIVER_OFFERS withParamData:dictParam withBlock:^(id response, NSError *error) {
            
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                if ([[response  objectForKey:@"success"] boolValue])
                {
                    if([response objectForKey:@"currency_sign"]) {
                        currencySign = [response objectForKey:@"currency_sign"];
                        NSLog(@"Sign : %@", currencySign);
                    } else {
                        currencySign = @"$";
                    }
                    
                    if([response objectForKey:@"current_country"]) {
                        currentCountry = [response objectForKey:@"current_country"];
                        NSLog(@"CC : %@", currentCountry);
                    } else {
                        currentCountry = @"Singapore";
                    }
                    
                    if (self.customSegmentedControl.selectedSegmentIndex == 0)
                    {
                        [arrNearest addObjectsFromArray:[response valueForKey:@"offers"]];
                        arrOffers = [arrNearest mutableCopy];
                        totalPagesNearest=[[response valueForKey:@"page_counts"]integerValue];
                        if(numberOfPage==totalPagesNearest)
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
                        }
                        else
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }
                    }
                    else
                    {
                        [arrEarliest addObjectsFromArray:[response valueForKey:@"offers"]];
                        arrOffers = [arrEarliest mutableCopy];
                        totalPagesEarliest=[[response valueForKey:@"page_counts"]integerValue];
                        if(numberOfPage==totalPagesEarliest)
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
                        }
                        else
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }
                    }
                    if ([arrOffers count] > 0)
                    {
                        [tableForOffers setHidden:NO];
                        [no_items setHidden:YES];
                        [tableForOffers reloadData];
                    }
                    else
                    {
                        [tableForOffers setHidden:YES];
                        [no_items setHidden:NO];
                    }
                    [pullToRefreshManager_ relocatePullToRefreshView];
                }
                else{
                    
                    if ([[response objectForKey:@"error"]isEqualToString:@"Error occurred , Please login again"] || [[response objectForKey:@"error"] isEqualToString:@"Not a valid token"]) {
                        
                        [super logoutAlert];
                    }else{
                        
                        NSLog(@"RO : %@", response);
                        
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"RIDE_OFFERS", nil) message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        
                        [alert show];
                        [arrOffers removeAllObjects];
                        [tableForOffers setHidden:YES];
                        [no_items setHidden:NO];
                    }
                }
            }
            else{
                [self getOffers:numberOfPage];
                [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
                [arrOffers removeAllObjects];
                [tableForOffers setHidden:YES];
                [no_items setHidden:NO];
            }
            [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [pullToRefreshManager_ tableViewScrolled];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}

@end
