//
//  RideOffersVC.h
//  SwiftBack
//
//  Created by Elluminati on 22/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "RideNextCell.h"
#import <NYSegmentedControl/NYSegmentedControl.h>
#import "BaseVC.h"
#import "Social/Social.h"
#import <MessageUI/MessageUI.h>
#import "MNMBottomPullToRefreshManager.h"

@interface RideOffersVC : BaseVC<UITableViewDataSource,UITableViewDelegate, MFMailComposeViewControllerDelegate, UIActionSheetDelegate,MNMBottomPullToRefreshManagerClient>
{
    MFMailComposeViewController *mailComposer;
	NSMutableArray *arrOffers;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
	__weak IBOutlet UITableView *tableForOffers;
	__weak IBOutlet UISegmentedControl *offersSegment;
	__weak IBOutlet UIButton *confirmBtn;
	__weak IBOutlet UIButton *menuBtn;
	__weak IBOutlet UIImageView *no_items;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblText1;
@property (weak, nonatomic) IBOutlet UILabel *lblText2;


@property (nonatomic, strong) NYSegmentedControl *customSegmentedControl;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *viewForFBSharing;

- (IBAction)onClickCloseShareView:(id)sender;
- (IBAction)onClickConfirmOffers:(id)sender;
- (IBAction)onClickSegment:(id)sender;
- (void)shareOnFacebook;
- (void)shareOnWhatsApp;
- (void)shareWithEmail;
- (void)showOptionToShare:(UITapGestureRecognizer *)tapGestureRecognizer;

@end
