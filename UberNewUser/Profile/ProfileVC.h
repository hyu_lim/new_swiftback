//
//  ProfileVC.h
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "RatingBar.h"

@interface ProfileVC : BaseVC <UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIScrollViewDelegate> {
    
    NSMutableArray *arrReviewList;
    NSDictionary *dictReview;
    NSMutableArray *arrRatingData;
}


@property (weak, nonatomic) IBOutlet UILabel *lblWelcomText;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileAlertText1;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileAlertText2;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblResetPwdTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnResetPassword;

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailInfo;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UITextField *txtOccupation;
@property (weak, nonatomic) IBOutlet UITextField *txtGender;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPWD;
//@property (weak, nonatomic) IBOutlet UITextField *txtNewPWD;
@property (weak, nonatomic) IBOutlet UITextField *txtConformPWD;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPwd;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPwd;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIButton *btnProPic;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIImageView *proPicImgv;
@property (weak, nonatomic) IBOutlet UIImageView *proPicImgv2;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollObj;
@property (strong, nonatomic) IBOutlet UIScrollView *horizontalScrollView;

@property (weak, nonatomic) IBOutlet UIView *viewForEmailInfo;
@property (weak, nonatomic) IBOutlet UIView *viewForResetPassword;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIView *viewForGreenOverlay;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *backgroundReset;
@property (weak, nonatomic) IBOutlet UIView *viewForProfilePicAlert;
@property (weak, nonatomic) IBOutlet UIView *blurView2;
@property (weak, nonatomic) IBOutlet UIView *viewForReview;

@property (strong, nonatomic) IBOutlet UIView *baseView;
@property (strong, nonatomic) IBOutlet UIImageView *clientImgView;
@property (strong, nonatomic) IBOutlet UIImageView *placeholderImgView;
@property (strong, nonatomic) IBOutlet UILabel *lblClientName;
@property (strong, nonatomic) IBOutlet UILabel *lblClientComment;
@property (strong, nonatomic) IBOutlet UIButton *btnLeft;
@property (strong, nonatomic) IBOutlet UIButton *btnRight;
@property (strong, nonatomic) IBOutlet RatingBar *clientRatingView;
@property (strong, nonatomic) IBOutlet UIImageView *headerBlurImageView;

- (IBAction)Logout:(id)sender;
- (IBAction)selectPhotoBtnPressed:(id)sender;
- (IBAction)updateBtnPressed:(id)sender;
- (IBAction)editBtnPressed:(id)sender;
- (IBAction)btnMenuClick:(id)sender;
- (IBAction)btnEmailInfoClick:(id)sender;
- (IBAction)onClickResetPassword:(id)sender;
- (IBAction)onClickResetOfPasswordView:(id)sender;
- (IBAction)onClickBtnCancelOfPasswordView:(id)sender;
- (IBAction)onClickCloseAlert:(id)sender;
- (void)slideUpAndDownForReviewWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer;


@end
