//
//  ProfileVC.m
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ProfileVC.h"
#import "UIImageView+Download.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVBase.h>
#import <AVFoundation/AVFoundation.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "UIView+Utils.h"
#import "SWRevealViewController.h"
#import "UberStyleGuide.h"
#import "RatingBar.h"

@interface ProfileVC ()
{
    NSString *strForUserId,*strForUserToken;
//    RatingBar *ratingView;
    NSMutableDictionary *dictInfo;
    
    //For fade in navigation bar
    // At this offset the Header stops its transformations
    CGFloat offset_HeaderStop;
    // At this offset the Black label reaches the Header
    CGFloat offset_B_LabelHeader;
    // The distance between the bottom of the Header and the top of the White Label
    CGFloat distance_W_LabelHeader;

}

@end

@implementation ProfileVC {
    NSInteger numberOfViews;
    NSString *commentString;
}

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad {
    
    //For fade in navigation bar
    offset_HeaderStop = 40.0;
    offset_B_LabelHeader = 10.0;
    distance_W_LabelHeader = 35.0;
    
    arrRatingData = [[NSMutableArray alloc] init];
    arrReviewList = [[NSMutableArray alloc] init];
    dictReview = [[NSDictionary alloc] init];
    //[super setNavBarTitle:TITLE_PROFILE];
    [super viewDidLoad];
    [self setLocalizedStrings];
    [super setBackBarItem];
    [self setDataForUserInfo];
    [self prepareHorizontalScrollView];
    [self customFont];
    [self customSetup];
    [self textDisable];
    [self layoutSetup];
    [self isProfileUploaded];
    
    //UITapGestureRecognizer to upload profile image from alert view
    UITapGestureRecognizer *profileNoticeTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectPhotoBtnPressed:)];
    [self.viewForProfilePicAlert addGestureRecognizer:profileNoticeTapGestureRecognizer];
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *reviewListTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpAndDownForReviewWithTapGestureRecognizer:)];
    
    [self.viewForReview addGestureRecognizer:reviewListTapGestureRecognizer];
    
    
    //For fade in navigation bar
    self.headerBlurImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.viewForNavigation.frame.size.width, self.viewForNavigation.frame.size.height)];
//    [self.headerBlurImageView downloadFromURL:[dictInfo valueForKey:@"picture"] withPlaceholder:nil];
    [self.headerBlurImageView setImage:[UIImage imageNamed:@"bg_green_for_navbar"]];
    
    self.headerBlurImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.headerBlurImageView.alpha = 0.0;
    [self.viewForNavigation addSubview: self.headerBlurImageView];
    
//    //blurView
//    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
//        self.headerBlurImageView.backgroundColor = [UIColor clearColor];
//        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//        
//        blurEffectView.frame = self.headerBlurImageView.bounds;
//        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        [self.headerBlurImageView addSubview:blurEffectView];
//        
//    } else {
//        self.headerBlurImageView.backgroundColor = [UIColor blackColor];
//    }
    
    self.viewForNavigation.clipsToBounds = YES;
    
    [self.viewForNavigation bringSubviewToFront:self.btnMenu];
    [self.viewForNavigation bringSubviewToFront:self.lblTitle];
    [self.viewForNavigation bringSubviewToFront:self.btnEdit];
    [self.viewForNavigation bringSubviewToFront:self.btnUpdate];
}

- (void)setLocalizedStrings {
    
    self.lblTitle.text=NSLocalizedString(@"PROFILE", nil);
    self.lblWelcomText.text=NSLocalizedString(@"WELCOME_BACK", nil);
    self.txtFirstName.placeholder=NSLocalizedString(@"FIRST_NAME", nil);
    self.txtLastName.placeholder=NSLocalizedString(@"LAST_NAME", nil);
    self.txtEmail.placeholder=NSLocalizedString(@"EMAIL", nil);
    self.txtPhone.placeholder=NSLocalizedString(@"PHONE", nil);
    self.txtGender.placeholder=NSLocalizedString(@"GENDER", nil);
    self.txtOccupation.placeholder=NSLocalizedString(@"OCCUPATION", nil);
    self.txtBio.placeholder=NSLocalizedString(@"BIO", nil);
    self.txtCurrentPWD.placeholder=NSLocalizedString(@"CURRENT_PASSWORD", nil);
    // self.txtNewPWD.placeholder=NSLocalizedString(@"NEW PASSWORD", nil);
    self.txtConformPWD.placeholder=NSLocalizedString(@"CONFIRM_PASSWORD", nil);
    [self.btnEdit setTitle:NSLocalizedString(@"EDIT_PROFILE", nil) forState:UIControlStateNormal];
    [self.btnUpdate setTitle:NSLocalizedString(@"UPDATE_PROFILE", nil) forState:UIControlStateNormal];
    [self.lblProfileAlertText1 setText:NSLocalizedString(@"COULD_NOT_GET_A_RIDE", nil)];
    [self.lblProfileAlertText2 setText:NSLocalizedString(@"TRY_ADDING_A_PROFILE_PHOTO", nil)];
    [self.lblReviewTitle setText:NSLocalizedString(@"REVIEWS", nil)];
    [self.lblResetPwdTitle setText:NSLocalizedString(@"RESET_PASSWORD",nil)];
    [self.btnReset setTitle:NSLocalizedString(@"RESET", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnResetPassword setTitle:NSLocalizedString(@"RESET_PASSWORD", nil) forState:UIControlStateNormal];
    [self.txtNewPwd setPlaceholder:NSLocalizedString(@"NEW_PASSWORD", nil)];
    [self.txtOldPwd setPlaceholder:NSLocalizedString(@"CURRENT_PASSWORD", nil)];
}

- (void)isProfileUploaded {
    
    NSLog(@"Profile Pic : %@", [dictInfo objectForKey:@"picture"]);
    
    if([[dictInfo objectForKey:@"picture"] isKindOfClass:[NSNull class]] || [[dictInfo objectForKey:@"picture"] isEqualToString:@""] || ![dictInfo objectForKey:@"picture"]) {
        [self.viewForProfilePicAlert setHidden:NO];
    } else {
        [self.viewForProfilePicAlert setHidden:YES];
    }
}

- (void)layoutSetup {

    self.viewForEmailInfo.hidden = YES;
    self.btnEdit.hidden = NO;
    self.btnUpdate.hidden = YES;
    
    [self.proPicImgv applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    
//    ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(80, 15) AndPosition:CGPointMake(120,175)];
//    ratingView.backgroundColor=[UIColor clearColor];
//    [self.scrollObj addSubview:ratingView];
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //rectangle background Reset
    _viewForResetPassword.layer.cornerRadius = 10;
    _viewForResetPassword.layer.borderWidth = 1;
    _viewForResetPassword.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewForResetPassword.layer.shadowRadius = 5.0;
    _viewForResetPassword.layer.shadowOpacity = 0.4;
    
    //rectangle background Reset
    _viewForReview.layer.cornerRadius = 10;
    _viewForReview.layer.borderWidth = 1;
    _viewForReview.layer.borderColor = [UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1].CGColor;
    _viewForReview.layer.shadowRadius = 3.0;
    _viewForReview.layer.shadowOpacity = 0.3;
    
    //rectangle background Reset
    _backgroundReset.layer.cornerRadius = 10;
    _backgroundReset.layer.borderWidth = 1;
    _backgroundReset.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundReset.layer.shadowRadius = 5.0;
    _backgroundReset.layer.shadowOpacity = 0.4;
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //viewForProfilePicAlert
    _viewForProfilePicAlert.layer.cornerRadius = 10;
    _viewForProfilePicAlert.layer.borderColor = [UIColor colorWithRed:216.f/255.f green:216.f/255.f blue:216.f/255.f alpha:0.7].CGColor;
    _viewForProfilePicAlert.layer.borderWidth = 1;
    _viewForProfilePicAlert.clipsToBounds = YES;

    //    //View Controller Clipping
    //    _mainView.layer.cornerRadius = 5;
    //    _mainView.clipsToBounds = YES;
    
    //blurView
    //_blurView.layer.cornerRadius = 10;
    _blurView2.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView2.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView2.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView2 addSubview:blurEffectView];
        
    } else {
        self.blurView2.backgroundColor = [UIColor blackColor];
    }

}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.scrollView setContentSize:CGSizeMake(320, 700)];
}

- (void)viewWillAppear:(BOOL)animate {
    //[self.txtFirstName setTintColor:[UIColor whiteColor]];
    //[self.txtLastName setTintColor:[UIColor whiteColor]];
    self.navigationController.navigationBarHidden=YES;
    self.viewForResetPassword.hidden=YES;
}

- (void)viewDidAppear:(BOOL)animate {
    [self.btnNavigation setTitle:NSLocalizedString(@"Profile", nil) forState:UIControlStateNormal];
}

- (void)setDataForUserInfo {
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
    NSLog(@"DICTINFO : %@", dictInfo);
    
    if([dictInfo valueForKey:@"rating_data"] != nil){
        [arrRatingData addObject:[dictInfo valueForKey:@"rating_data"]];
        NSLog(@"ArrRatingData : %@", arrRatingData);
        numberOfViews = [arrRatingData[0][@"rating_list"] count];
        [arrReviewList addObject:[arrRatingData[0] objectForKey:@"rating_list"]];
        NSLog(@"RATING LIST COUNT ===> %li", (long)numberOfViews);
        
        [self.ratingView initRateBar];
        [self.ratingView setUserInteractionEnabled: NO];
        
        RBRatings rating = (float)([[arrRatingData[0] valueForKey:@"rating"] floatValue] * 2);
        [self.ratingView setRatings:rating];
    } else {
        numberOfViews = 0;
        
        [self.ratingView initRateBar];
        [self.ratingView setUserInteractionEnabled: NO];
        
        RBRatings rating = 0;
        [self.ratingView setRatings:rating];
    }
    
    [self.proPicImgv downloadFromURL:[dictInfo valueForKey:@"picture"] withPlaceholder:nil];
    [self.proPicImgv2 downloadFromURL:[dictInfo valueForKey:@"picture"] withPlaceholder:nil];
    self.txtFirstName.text=[dictInfo valueForKey:@"first_name"];
    self.userName.text=[NSString stringWithFormat:@"%@!", [dictInfo valueForKey:@"first_name"]];
    self.txtLastName.text=[dictInfo valueForKey:@"last_name"];
    self.txtEmail.text=[dictInfo valueForKey:@"email"];
    self.txtPhone.text=[dictInfo valueForKey:@"phone"];
    self.lblEmailInfo.text=NSLocalizedString(@"This field is not editable.", nil);
	self.txtOccupation.text = [dictInfo valueForKey:@"occupation"];
    self.txtGender.text = [dictInfo valueForKey:@"gender"];
    //self.txtAddress.text=[dictInfo valueForKey:@"address"];
    ///self.txtZipCode.text=[dictInfo valueForKey:@"zipcode"];
    self.txtBio.text=[dictInfo valueForKey:@"bio"];
}

- (void)customSetup {
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    }
}

//+++++++++++++++++++++++++++++++++++++//
- (void) prepareHorizontalScrollView {
    
    CGRect scrollViewFrame = CGRectMake(0, 25, self.viewForReview.frame.size.width, 100);
    self.horizontalScrollView.frame = scrollViewFrame;
    self.horizontalScrollView.pagingEnabled = YES;
    self.horizontalScrollView.bounces = NO;
    
    
    //Create left button and add to the subview
    CGRect leftBtnFrame = CGRectMake(10, 70, 20, 20);
    self.btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnLeft addTarget:self action:@selector(onClickLefBtn) forControlEvents:UIControlEventTouchUpInside];
    self.btnLeft.frame = leftBtnFrame;
    [self.btnLeft setImage:[UIImage imageNamed:@"icon_left_arrow_black"] forState:UIControlStateNormal];
    [self.viewForReview addSubview:self.btnLeft];
    
    
    
    //Create right button and add to the subview
    CGRect rightBtnFrame = CGRectMake((self.viewForReview.frame.size.width - 40), 70, 20, 20);
    self.btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnRight addTarget:self action:@selector(onClickRightBtn) forControlEvents:UIControlEventTouchUpInside];
    self.btnRight.frame = rightBtnFrame;
    [self.btnRight setImage:[UIImage imageNamed:@"icon_right_arrow_black"] forState:UIControlStateNormal];
    [self.viewForReview addSubview:self.btnRight];
    
    NSLog(@"ArrReviewList ===> %@", arrReviewList);
    
    
    //Init RatingBar here (P.S. cannnot initialize it inside of loop, if we do, stars won't be filled)
    [self.clientRatingView initRateBar];
    
    if(numberOfViews == 0) {
        CGRect placeholderFrame = CGRectMake(30, 15, self.viewForReview.frame.size.width - 60, self.viewForReview.frame.size.height - 10);
        self.placeholderImgView = [[UIImageView alloc] initWithFrame:placeholderFrame];
        [self.placeholderImgView setImage:[UIImage imageNamed:@"icon_no_review.png"]];
        [self.viewForReview addSubview:self.placeholderImgView];
        [self.viewForReview bringSubviewToFront:self.viewForReview];
    } else {
        for(int i=0; i<numberOfViews; i++) {
            
            dictReview = arrReviewList[0][i];
            //        NSLog(@"FirstName : %@",[dictReview objectForKey:@"first_name"]);
            //        NSLog(@"LastName : %@",[dictReview objectForKey:@"last_name"]);
            //        NSLog(@"ID : %@",[dictReview objectForKey:@"id"]);
            //        NSLog(@"Rating : %@",[dictReview objectForKey:@"rating"]);
            //        NSLog(@"Picture : %@",[dictReview objectForKey:@"picture"]);
            //        NSLog(@"Comment : %@",[dictReview objectForKey:@"comment"]);
            
            //Set the origin of the sub view
            CGFloat myOrigin = i * self.viewForReview.frame.size.width;
            
            //Create the sub view and allocate memory
            self.baseView = [[UIView alloc] initWithFrame:CGRectMake(myOrigin, 0, self.horizontalScrollView.frame.size.width, self.horizontalScrollView.frame.size.height)];
            //Set the background to clear color
            self.baseView.backgroundColor = [UIColor clearColor];
            
            
            //Create a imageView and add to the subview
            CGRect imgViewFrame = CGRectMake(50.f, 20.f, 70.f, 70.f);
            self.clientImgView = [[UIImageView alloc] initWithFrame:imgViewFrame];
            [self.clientImgView downloadFromURL:[dictReview objectForKey:@"picture"]withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
            [self.clientImgView applyRoundedCornersFull];
            [self.baseView addSubview:self.clientImgView];
            
            
            //Create a label and add to the subview
            CGRect nameFrame = CGRectMake(130.0f, 15.0f, 200.0f, 21.0f);
            self.lblClientName = [[UILabel alloc]initWithFrame:nameFrame];
            self.lblClientName.text = [NSString stringWithFormat:@"%@", [[dictReview objectForKey:@"first_name"] capitalizedString]];
            [self.lblClientName setFont:[UIFont fontWithName:@"OpenSans-Light" size:13.f]];
            self.lblClientName.textColor = [UIColor darkGrayColor];
            [self.baseView addSubview:self.lblClientName];
            
            
            //Create RatingBar
            self.clientRatingView = [[RatingBar alloc] initWithSize:CGSizeMake(70.f, 13.f) AndPosition:CGPointMake(130.f, 37.f)];
            self.clientRatingView.backgroundColor = [UIColor clearColor];
            //        [self.clientRatingView initRateBar];
            [self.clientRatingView setUserInteractionEnabled:NO];
            RBRatings rating = (float)([[dictReview objectForKey:@"rating"] floatValue] *2);
            //        NSLog(@"Rating ==> %f", (float)([[dictReview objectForKey:@"rating"] floatValue] *2));
            [self.clientRatingView setRatings:rating];
            [self.baseView addSubview:self.clientRatingView];
            
            
            //Create a label and add to the subview
            CGRect commentFrame = CGRectMake(130.0f, 53.0f, 150.0f, 40.0f);
            self.lblClientComment = [[UILabel alloc]initWithFrame:commentFrame];
            
            commentString = [dictReview objectForKey:@"comment"];
            if([commentString isEqualToString:@""] || [commentString isKindOfClass:[NSNull class]] || !commentString){
                commentString = @"No Review";
            }
            self.lblClientComment.text = [NSString stringWithFormat:@"%@", commentString];
            [self.lblClientComment setFont:[UIFont fontWithName:@"OpenSans-Light" size:11.f]];
            [self.lblClientComment setNumberOfLines:0];
            [self.lblClientComment sizeToFit];
            [self.lblClientComment setLineBreakMode:NSLineBreakByWordWrapping];
            self.lblClientComment.textColor = [UIColor darkGrayColor];
            [self.baseView addSubview:self.lblClientComment];
            
            
            //Set the scroll view
            self.horizontalScrollView.delegate = self;
            [self.horizontalScrollView addSubview:self.baseView];
            self.horizontalScrollView.contentSize = CGSizeMake(self.viewForReview.frame.size.width * numberOfViews, self.horizontalScrollView.frame.size.height);
            [self.viewForReview addSubview:self.horizontalScrollView];
        }
    }
    
    if((numberOfViews == 0) || (numberOfViews == 1)) {
        [self.btnLeft setHidden:YES];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:YES];
    }
    
    [self.viewForReview bringSubviewToFront:self.btnLeft];
    [self.viewForReview bringSubviewToFront:self.btnRight];
}

- (void)onClickLefBtn {
    CGFloat pageWidth = self.horizontalScrollView.frame.size.width;
    CGFloat offset = self.horizontalScrollView.contentOffset.x - pageWidth;
    [self.horizontalScrollView setContentOffset:CGPointMake(offset, 0)];
    
    if(offset == 0.0) {
        [self.btnLeft setHidden:YES];
        [self.btnRight setHidden:NO];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.horizontalScrollView.layer addAnimation:transition forKey:nil];
}

- (void)onClickRightBtn {
    CGFloat pageWidth = self.horizontalScrollView.frame.size.width;
    CGFloat offset = self.horizontalScrollView.contentOffset.x + pageWidth;
    CGFloat mainViewFrameSize = self.view.frame.size.width * (numberOfViews-1);
    [self.horizontalScrollView setContentOffset:CGPointMake(offset, 0)];
    
    if(offset == mainViewFrameSize) {
        [self.btnRight setHidden:YES];
        [self.btnLeft setHidden:NO];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.horizontalScrollView.layer addAnimation:transition forKey:nil];
}

// scrolling ends
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth/2)/pageWidth) + 1;
    
    if(page == 0) {
        [self.btnRight setHidden:NO];
        [self.btnLeft setHidden:YES];
    } else if(page == numberOfViews-1) {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
}

// dragging ends
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth/2)/pageWidth) + 1;
    
    if(page == 0) {
        [self.btnRight setHidden:NO];
        [self.btnLeft setHidden:YES];
    } else if(page == numberOfViews-1) {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
}

- (void)slideUpAndDownForReviewWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    
    CGFloat newYCoordinate = 525.0;
    
    if(self.viewForReview.frame.origin.y == 525.0) {
        newYCoordinate = 418.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        self.viewForReview.frame = CGRectMake(self.viewForReview.frame.origin.x, newYCoordinate, self.viewForReview.frame.size.width, self.viewForReview.frame.size.height);
    }];
}

//+++++++++++++++++++++++++++++++++++++//

//For fade in navigation bar
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offset = scrollView.contentOffset.y;
    self.headerBlurImageView.alpha = MIN(1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader);
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark - UIButton Action

- (IBAction)btnMenuClick:(id)sender {
    
}

- (IBAction)btnEmailInfoClick:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==0)
    {
        btn.tag=1;
        self.viewForEmailInfo.hidden=NO;
    }
    else
    {
        btn.tag=0;
        self.viewForEmailInfo.hidden=YES;
    }
    
}

- (IBAction)selectPhotoBtnPressed:(id)sender {
    
    [self.view endEditing:YES];
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    UIActionSheet *actionpass;
    
    actionpass = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"SELECT_PHOTO", @""),NSLocalizedString(@"TAKE_PHOTO", @""),nil];
    actionpass.delegate=self;
    [actionpass showInView:window];
}

- (IBAction)updateBtnPressed:(id)sender {
    if (self.txtNewPwd.text.length > 0 || self.txtConformPWD.text.length > 0)
    {
        if ([self.txtNewPwd.text isEqualToString:self.txtConformPWD.text])
        {
            [self updateProfile];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Profile Update Fail" message:NSLocalizedString(@"NOT_MATCH_RETYPE",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        [self updateProfile];
    }
    
}

-(void)updateProfile {
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        if([[UtilityClass sharedObject]isValidEmailAddress:self.txtEmail.text])
        {
            
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"EDITING", nil)];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:self.txtEmail.text forKey:PARAM_EMAIL];
            [dictParam setValue:self.txtFirstName.text forKey:PARAM_FIRST_NAME];
            [dictParam setValue:self.txtLastName.text forKey:PARAM_LAST_NAME];
            [dictParam setValue:self.txtPhone.text forKey:PARAM_PHONE];
            [dictParam setValue:@"" forKey:PARAM_BIO];
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:self.txtOccupation.text forKey:PARAM_OCCUPATION];
            [dictParam setValue:self.txtBio.text forKey:PARAM_BIO];
            
            [dictParam setValue:@"" forKey:PARAM_ADDRESS];
            [dictParam setValue:@"" forKey:PARAM_STATE];
            [dictParam setValue:@"" forKey:PARAM_COUNTRY];
            [dictParam setValue:@"" forKey:PARAM_ZIPCODE];
            
            
            UIImage *imgUpload = [[UtilityClass sharedObject]scaleAndRotateImage:self.proPicImgv.image];
            
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_UPADTE withParamDataImage:dictParam andImage:imgUpload withBlock:^(id response, NSError *error) {
                
                [[AppDelegate sharedAppDelegate]hideLoadingView];
                if (response)
                {
                    if([[response valueForKey:@"success"] boolValue])
                    {
                        
                        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                        [pref setObject:response forKey:PREF_LOGIN_OBJECT];
                        [pref synchronize];
                        [self setDataForUserInfo];
                        [APPDELEGATE showToastMessage:NSLocalizedString(@"PROFILE_EDIT_SUCESS", nil)];
                        [self textDisable];
                        self.btnUpdate.hidden=YES;
                        self.btnEdit.hidden=NO;
                        self.txtConformPWD.text=@"";
                        self.txtCurrentPWD.text=@"";
                       // self.txtNewPWD.text=@"";
                        // [self.navigationController popViewControllerAnimated:YES];
                    }
                    else
                    {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
                
                NSLog(@"REGISTER RESPONSE --> %@",response);
            }];
        }
        
        
    }
    
    else
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }


}

- (IBAction)editBtnPressed:(id)sender {
    
    [self textEnable];
    [self.btnEdit setHidden:YES];
    [self.btnUpdate setHidden:NO];
    [self.txtFirstName becomeFirstResponder];
    [APPDELEGATE showToastMessage:@"You Can Edit Your Profile"];

}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont {
    self.txtFirstName.font=[UberStyleGuide fontRegular];
    self.txtLastName.font=[UberStyleGuide fontRegular];
    self.txtPhone.font=[UberStyleGuide fontRegular];
    self.txtEmail.font=[UberStyleGuide fontRegular];
   // self.txtAddress.font=[UberStyleGuide fontRegular];
   // self.txtBio.font=[UberStyleGuide fontRegular];
   // self.txtZipCode.font=[UberStyleGuide fontRegular];
    
    self.btnNavigation.titleLabel.font=[UberStyleGuide fontRegular];
   //self.btnEdit.titleLabel.font=[UberStyleGuide fontRegularBold];
    //self.btnUpdate.titleLabel.font=[UberStyleGuide fontRegularBold];
}

-(void)textDisable {
    
    self.txtFirstName.enabled = NO;
    self.txtLastName.enabled = NO;
    self.txtEmail.enabled = NO;
    self.txtPhone.enabled = NO;
    self.txtConformPWD.enabled=NO;
    self.txtCurrentPWD.enabled=NO;
   // self.txtNewPWD.enabled=NO;
   // self.txtAddress.enabled = NO;
   // self.txtZipCode.enabled = NO;
   // self.txtBio.enabled = NO;
    self.btnProPic.enabled=NO;
    self.txtGender.enabled = NO;
    self.txtOccupation.enabled = NO;
    self.txtBio.enabled = NO;
    
    CGPoint offset;
    offset = CGPointMake(0, 0);
    [self.scrollView setContentSize:CGSizeMake(320, 700)];
    [self.scrollView setContentOffset:offset animated:YES];
}

-(void)textEnable{
    
    self.txtFirstName.enabled = YES;
    self.txtLastName.enabled = YES;
    self.txtEmail.enabled = NO;
    self.txtPhone.enabled = YES;
    self.txtConformPWD.enabled=YES;
    self.txtCurrentPWD.enabled=YES;
  //  self.txtNewPWD.enabled=YES;
   // self.txtAddress.enabled = YES;
   // self.txtZipCode.enabled = YES;
   // self.txtBio.enabled = YES;
    self.btnProPic.enabled=YES;
    self.txtGender.enabled = NO;
    self.txtOccupation.enabled = YES;
    self.txtBio.enabled = YES;
    
    [self.scrollView setContentSize:CGSizeMake(320, 850)];
}

//#pragma mark
//#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (buttonIndex)
    {
        case 1:
        {
            [self takePhoto];
        }
            break;
        case 0:
        {
            [self selectPhotos];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark
#pragma mark - Action to Share

- (void)selectPhotos {
    // Set up the image picker controller and add it to the view
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing=YES;
    [self presentViewController:imagePickerController animated:YES completion:^{
       
    }];
}

-(void)takePhoto
{
    // Set up the image picker controller and add it to the view
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType =UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing=YES;
        [self presentViewController:imagePickerController animated:YES completion:^{
            [self updateProfile];
            [_viewForProfilePicAlert setHidden:YES];
        }];
    } else {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
    }
}

#pragma mark
#pragma mark - ImagePickerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if([info valueForKey:UIImagePickerControllerEditedImage]==nil)
    {
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        [assetLibrary assetForURL:[info valueForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
            ALAssetRepresentation *rep = [asset defaultRepresentation];
            Byte *buffer = (Byte*)malloc(rep.size);
            NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
            NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
            UIImage *img=[UIImage imageWithData:data];
            [self setImage:img];
        } failureBlock:^(NSError *err) {
            NSLog(@"Error: %@",[err localizedDescription]);
        }];
    }
    else
    {
        [self.viewForProfilePicAlert setHidden:YES];
        [self setImage:[info valueForKey:UIImagePickerControllerEditedImage]];
        [self updateProfile];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)setImage:(UIImage *)image
{
    self.proPicImgv.image=image;
    self.proPicImgv2.image=image;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark - UItextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
//    CGPoint offset;
//    
//    if(textField==self.txtFirstName){
//        offset=CGPointMake(0, 20);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtLastName) {
//        offset=CGPointMake(0, 20);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtPhone) {
//        offset=CGPointMake(0, 80);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtOccupation) {
//        offset=CGPointMake(0, 160);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtBio) {
//        offset=CGPointMake(0, 200);
//        [self.scrollView setContentOffset:offset animated:YES];
//    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    CGPoint offset;
    
//    if(textField==self.txtFirstName){
//        offset=CGPointMake(0, 222);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtLastName) {
//        offset=CGPointMake(0, 222);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtPhone) {
//        offset=CGPointMake(0, 300);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtOccupation) {
//        offset=CGPointMake(0, 380);
//        [self.scrollView setContentOffset:offset animated:YES];
//    } else if(textField==self.txtBio) {
//        offset=CGPointMake(0, 420);
//        [self.scrollView setContentOffset:offset animated:YES];
//    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    /*if (textField==self.txtFirstName)
    {
        [self.txtLastName becomeFirstResponder];
    }
    if (textField==self.txtLastName)
    {
         //[[UITextField appearance] setTintColor:[UIColor blackColor]];
        [self.txtEmail becomeFirstResponder];
    }
    if (textField==self.txtEmail)
    {
        [self.txtPhone becomeFirstResponder];
    }
    if (textField==self.txtPhone)
    {
     
        [self.txtAddress becomeFirstResponder];
    }
    if (textField==self.txtAddress)
    {
        [self.txtBio  becomeFirstResponder];
    }
    if (textField==self.txtBio)
    {
        [self.txtZipCode becomeFirstResponder];
    }*/

    
//    [UIView animateWithDuration:0.5 animations:^{
//        
//        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        
//    } completion:^(BOOL finished)
//     {
//     }];
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)onClickResetPassword:(id)sender
{
    
    _viewForGreenOverlay.alpha = 0;
    _viewForResetPassword.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewForGreenOverlay.alpha = 0.8;
        self.viewForResetPassword.alpha = 1;
        self.viewForGreenOverlay.hidden = NO;
        self.viewForResetPassword.hidden = NO;
        [self.txtNewPwd resignFirstResponder];
        [self.txtOldPwd resignFirstResponder];
    }];
    
}

- (IBAction)onClickResetOfPasswordView:(id)sender
{
    [self.txtNewPwd resignFirstResponder];
    [self.txtOldPwd resignFirstResponder];
    if([[AppDelegate sharedAppDelegate]connected])
    {
        if(self.txtNewPwd.text.length > 0 && self.txtOldPwd.text.length > 0)
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:@"Password Updating..."];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setObject:self.txtOldPwd.text forKey:@"old_password"];
            [dictParam setObject:self.txtNewPwd.text forKey:@"new_password"];
        
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RESET_PASSWORD withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         self.viewForGreenOverlay.hidden = YES;
                         self.viewForResetPassword.hidden=YES;
                         self.txtOldPwd.text=@"";
                         self.txtNewPwd.text=@"";
                         [self.txtNewPwd resignFirstResponder];
                         [self.txtOldPwd resignFirstResponder];
                         [[AppDelegate sharedAppDelegate] showToastMessage:[response valueForKey:@"message"]];
                     }
                     else
                     {
                         self.txtOldPwd.text=@"";
                         self.txtNewPwd.text=@"";
                          [[AppDelegate sharedAppDelegate] showToastMessage:[response valueForKey:@"message"]];
                     }
                 }
             
             
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Please enter your old and new password.", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }

    
}

- (IBAction)onClickBtnCancelOfPasswordView:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewForGreenOverlay.alpha = 0;
        _viewForResetPassword.alpha = 0;
    } completion:^(BOOL finished){
        [self.viewForGreenOverlay setHidden: finished];
        [self.viewForResetPassword setHidden: finished];
        self.txtOldPwd.text=@"";
        self.txtNewPwd.text=@"";
        [self.txtNewPwd resignFirstResponder];
        [self.txtOldPwd resignFirstResponder];
    }];
}

- (IBAction)onClickCloseAlert:(id)sender {
    
    [self.viewForProfilePicAlert setHidden:YES];
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.5;
    
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition.type = kCATransitionFade;
    
    transition.delegate = self;
    
    [self.viewForProfilePicAlert.layer addAnimation:transition forKey:nil];
}

/*
 - (IBAction)Logout:(id)sender {
    
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        [pref removeObjectForKey:PREF_USER_TOKEN];
        [pref removeObjectForKey:PREF_REQ_ID];
        [pref removeObjectForKey:PREF_IS_LOGOUT];
        [pref removeObjectForKey:PREF_USER_ID];
        [pref removeObjectForKey:PREF_IS_LOGIN];
        [pref synchronize];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        //  if([[AppDelegate sharedAppDelegate]connected])
        //            {
        //                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        //                [afn getDataFromPath:FILE_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
        //                 {
        //                     [[AppDelegate sharedAppDelegate]hideLoadingView];
        //                     if (response)
        //                     {
        //                         [pref removeObjectForKey:PREF_USER_TOKEN];
        //                         [pref removeObjectForKey:PREF_REQ_ID];
        //                         [pref removeObjectForKey:PREF_IS_LOGOUT];
        //                         [pref removeObjectForKey:PREF_USER_ID];
        //                         [pref removeObjectForKey:PREF_IS_LOGIN];
        //                         [pref synchronize];
        //                         [self.navigationController popToRootViewControllerAnimated:YES];
        //                     }
        //
        //
        //                 }];
        //                
        //            }
    

}
 */

@end
