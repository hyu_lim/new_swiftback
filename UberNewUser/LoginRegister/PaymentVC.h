//
//  PaymentVC.h
//  UberNew
//
//  Developed by Elluminati on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "CardIOPaymentViewControllerDelegate.h"
#import "PTKView.h"

#import <UIKit/UIKit.h>
#import "PaymentPageContentViewController.h"

@protocol STPPaymentCardTextFieldDelegate <NSObject>

@end

@interface PaymentVC : BaseVC<UITextFieldDelegate,UIAlertViewDelegate,UIPageViewControllerDataSource>

@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *pageTitles2;
@property (strong, nonatomic) NSArray *pageImages2;
@property (strong, nonatomic) NSArray *pageTitles3;

@property (nonatomic,strong) NSString *strForPayment;
@property (nonatomic,strong) NSString *strForID;
@property (nonatomic,strong) NSString *strForToken;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCardAccepted;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentText;
@property (weak, nonatomic) IBOutlet UILabel *lblAutomaticText;
@property (weak, nonatomic) IBOutlet UILabel *lblSecuredText;
@property (weak, nonatomic) IBOutlet UILabel *lblSupportedText;

@property (weak, nonatomic) IBOutlet UIButton *btnAddPayment;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *skipBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (strong, nonatomic) UIPageViewController *pageViewController;

@property (weak, nonatomic) PTKView *paymentView;
@property (weak, nonatomic) id <STPPaymentCardTextFieldDelegate> delegate;

- (IBAction)scanBtnPressed:(id)sender;
- (IBAction)addPaymentBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)STPPaymentCardTextField:(UITextField *)sender;
- (IBAction)onClickBackButton:(id)sender;
- (IBAction)startWalkthrough:(id)sender;

@end
