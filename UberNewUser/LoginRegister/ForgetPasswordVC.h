//
//  ForgetPasswordVC.h
//  UberforXOwner
//
//  Created by Deep Gami on 14/11/14.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "BaseVC.h"

@interface ForgetPasswordVC : BaseVC <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (strong, nonatomic) IBOutlet UIView *scrLogin;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundRect;

- (IBAction)signBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)onClickBackBtnPressed:(id)sender;

@end
