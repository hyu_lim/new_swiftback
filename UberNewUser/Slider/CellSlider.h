//
//  CellSlider.h
//  UberNewUser
//
//  Developed by Elluminati on 30/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellSlider : UITableViewCell
{
    id cellData;
    id cellParent;
}

@property(nonatomic,weak)IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblChatCounter;

@property (weak, nonatomic) IBOutlet UIView *cellSlider;
@property(nonatomic,weak)IBOutlet UIImageView *imgIcon;

-(void)setCellData:(id)data withParent:(id)parent;

@end
