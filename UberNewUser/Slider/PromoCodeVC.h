//
//  PromoCodeVC.h
//  SwiftBack Driver
//
//  Created by My Mac on 10/2/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import "BaseVC.h"
#import "Social/Social.h"
#import <MessageUI/MessageUI.h>

@interface PromoCodeVC : BaseVC<UITextFieldDelegate>
{
    __weak IBOutlet UITextField *txtPromoCode;
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UILabel *lblReferralCode;
    __weak IBOutlet UIView *viewForNavigation;
    __weak IBOutlet UIView *viewForReferral;
    __weak IBOutlet UIView *viewForApply;
    __weak IBOutlet UIView *viewForScreenshot;
    __weak IBOutlet UIImageView *backgroundRect;
    __weak IBOutlet UIImageView *backgroundRect2;
    __weak IBOutlet UIImageView *backgroundRectShare;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPromoCodeText;
@property (weak, nonatomic) IBOutlet UILabel *lblApplyPromoCodeText;
@property (weak, nonatomic) IBOutlet UILabel *lblSharePromoCodeText;
@property (weak, nonatomic) IBOutlet UILabel *lblGetYourRideText;
@property (weak, nonatomic) IBOutlet UILabel *lblUseReferralCodeText;
@property (weak, nonatomic) IBOutlet UILabel *promoDeadline;
@property (weak, nonatomic) IBOutlet UILabel *useReferralCode;
@property (weak, nonatomic) IBOutlet UILabel *userReferralCode;
@property (weak, nonatomic) IBOutlet UILabel *textShare;
@property (weak, nonatomic) IBOutlet UILabel *hideTitle;
@property (weak, nonatomic) IBOutlet UILabel *pushHeadline;

@property (weak, nonatomic) IBOutlet UITextField *txtPromoCode;

@property (weak, nonatomic) IBOutlet UIButton *btnRefer;
@property (weak, nonatomic) IBOutlet UIButton *btnReferralCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *buttonShare;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;

@property (weak, nonatomic) IBOutlet UIImageView *iconShare;
@property (weak, nonatomic) IBOutlet UIImageView *headerRect;
@property (weak, nonatomic) IBOutlet UIImageView *hideLogo;
@property (weak, nonatomic) IBOutlet UIImageView *hideHeader;

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIScrollView *viewForApply;
@property (weak, nonatomic) IBOutlet UIWebView *webView;


- (IBAction)onClickReferPromo:(id)sender;
- (IBAction)onClickReferralCode:(id)sender;
- (IBAction)onClickSubmit:(id)sender;

@end
