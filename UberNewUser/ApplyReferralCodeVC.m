//
//  ApplyReferralCodeVC.m
//  UberforXOwner
//
//  Created by Deep Gami on 22/11/14.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "ApplyReferralCodeVC.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "UberStyleGuide.h"
#import "PaymentVC.h"

@interface ApplyReferralCodeVC ()
{
    NSString *Referral;
    BOOL is_apply;
}
@end

@implementation ApplyReferralCodeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self layoutSetup];
    [self setLocalizedStrings];
    [self setNeedsStatusBarAppearanceUpdate];
    [self setNeedsStatusBarAppearanceUpdate];
    
    Referral=@"";
    is_apply=NO;
    
    self.viewForReferralError.hidden=YES;
    self.navigationItem.hidesBackButton=YES;
    
    //self.txtCode.font=[UberStyleGuide fontRegular];
    self.btnSubmit=[APPDELEGATE setBoldFontDiscriptor:self.btnSubmit];
    self.btnContinue=[APPDELEGATE setBoldFontDiscriptor:self.btnContinue];
    
    //set tap gesture recognizer
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    
    [self.viewForReferralError addGestureRecognizer:singleTapGestureRecognizer];
}

-(void)setLocalizedStrings
{
    self.lblTitle.text=NSLocalizedString(@"REFERRAL_CODE", nil);
    self.lblSharingText.text = NSLocalizedString(@"POWERED_BY_SHARING", nil);
    self.lblReferralCodeText.text = NSLocalizedString(@"REFERRAL_CODE_TEXT", nil);
    
    self.txtCode.placeholder=NSLocalizedString(@"ENTER_REFERRAL_CODE", nil);
    
    self.lblMsg.text=NSLocalizedString(@"REFERRAL_MSG", nil);
    
    [self.btnContinue setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateNormal];
    [self.btnContinue setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateSelected];
    
    [self.btnSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateSelected];
}

- (void)layoutSetup {
    
    [self.txtCode setValue:[UIColor colorWithRed:0.60 green:0.60 blue:0.60 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
}

//UPDATE STATUS BAR
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
//

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scrollView2 setContentSize:CGSizeMake(320, 1700)];
}

-(void)viewWillAppear:(BOOL)animated
{
     self.viewForReferralError.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.btn_Navi_Title setTitle:NSLocalizedString(@"REFERRAL_CODE", nil) forState:UIControlStateNormal];
}

-(void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    if(is_apply)
    {
        self.viewForReferralError.hidden=YES;
        [self performSegueWithIdentifier:@"segueToAddPayment" sender:self];
    }
    else
    {
        self.viewForReferralError.hidden=YES;
    }
}

- (IBAction)onClickSubmit:(id)sender {
    
    if ([APPDELEGATE connected])
    {
        if ([[[UtilityClass sharedObject] trimString:self.txtPromoCode.text] length ]< 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"ENTER_REFERRAL_CODE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"APPLYING_PROMOCODE", nil)];
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setObject:self.txtPromoCode.text forKey:PARAM_REFERRAL_CODE];
            
            AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [helper getDataFromPath:FILE_APPLY_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error) {
                [APPDELEGATE hideLoadingView];
                if (response)
                {
                    if ([[response objectForKey:@"success"]boolValue])
                    {
                        [APPDELEGATE showToastMessage:NSLocalizedString(@"PROMOCODE_APPLIED_SUCCESSFULLY", nil)];
                        [self performSegueWithIdentifier:@"segueToAddPayment" sender:self];
                        Referral=@"1";
                    }
                    else
                    {
                        [APPDELEGATE showToastMessage:[response objectForKey:@"error"]];
                    }
                }
            }];
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

/*
- (IBAction)codeBtnPressed:(id)sender
{
    Referral=@"0";
    [self createService];
}
 
 */

- (IBAction)ContinueBtnPressed:(id)sender
{
    [self performSegueWithIdentifier:@"segueToAddPayment" sender:self];
    Referral=@"1";
    [USERDEFAULT setBool:YES forKey:@"Skip"];
    [USERDEFAULT synchronize];
    //[self createService];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PaymentVC *pvc = [segue destinationViewController];
    pvc.strForPayment=@"1";
}
/*
 -(void)createService
{
    self.viewForReferralError.hidden=YES;
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:strForUserId forKey:PARAM_ID];
        [dictParam setObject:self.txtCode.text forKey:PARAM_REFERRAL_CODE];
        [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setObject:Referral forKey:PARAM_REFERRAL_SKIP];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_APPLY_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 self.txtCode.text=@"";
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"%@",response);
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                         [pref synchronize];
                         is_apply=YES;
                         self.img_code.image=[UIImage imageNamed:@"code_valid"];
                         self.lblReferralErrorMsg.text=@"Referral Code Accepted!";
                         if([Referral isEqualToString:@"0"])
                         {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"SUCESS_REFERRAL", nil)];
                        // [self performSegueWithIdentifier:@"segueToAddPayment" sender:self];
                         }
                         else
                         {
                             // [self performSegueWithIdentifier:@"segueToAddPayment" sender:self];
                         }
                     }
                 }
                 else
                 {
                     self.txtCode.text=@"";
                     is_apply=NO;
                     self.viewForReferralError.hidden=NO;
                     self.img_code.image=[UIImage imageNamed:@"code_invalid"];
                     self.lblReferralErrorMsg.text=[response valueForKey:@"error"];
                   //  self.lblReferralErrorMsg.textColor=[UIColor colorWithRed:205.0/255.0 green:0.0/255.0 blue:15.0/255.0 alpha:1];
                 }
             }
             
             
         }];
    }
    else
    {
       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }


}
 */
#pragma mark-
#pragma mark- TextField Delegate



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtCode resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint offset;
    if(textField==self.txtCode)
    {
        offset=CGPointMake(0, 200);
        [self.scrollView setContentOffset:offset animated:YES];
    }
    self.viewForReferralError.hidden=YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.scrollView setContentOffset:offset animated:YES];
    
    if(textField==self.txtCode)
        [self.txtCode becomeFirstResponder];
    //    else if(textField==self.txtNumber)
    //        [self.txtAddress becomeFirstResponder];
    //    else if(textField==self.txtAddress)
    //        [self.txtBio becomeFirstResponder];
    //    else if(textField==self.txtBio)
    //        [self.txtZipCode becomeFirstResponder];
    
    
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)onClickBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
