//
//  UserCell.m
//  SwiftBack
//
//  Created by Elluminati on 25/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "UserCell.h"
#import "UIView+Utils.h"

@implementation UserCell

- (void)awakeFromNib {
	
	[self.driverProfileView applyRoundedCornersFullWithColor:[UberStyleGuide colorDefault]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
