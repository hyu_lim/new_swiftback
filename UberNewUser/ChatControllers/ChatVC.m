#import "ChatVC.h"
#import "MessageTableViewCell.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"

typedef NS_ENUM(int, MessageDirection) {
  Incoming,
  Outgoing,
};

@interface ChatVC ()
{
  NSMutableArray *_messages;
  NSTimer *timerForChat;
  BOOL is_first,is_first_load;
  CGFloat tablePosition;
    
    // for Timestamp
    NSMutableArray *arrForDate;
    NSMutableArray *arrForSection;
}
@end

@implementation ChatVC
@synthesize tableForChat,viewForMessage;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)awakeFromNib
{
  _messages = [NSMutableArray array];
}

-(void)viewDidLoad
{
	[super viewDidLoad];
    [self layoutSetup];
    [self setLocalizedStrings];
	
    
    _messages=[[NSMutableArray alloc] init];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWasShown:)
												 name:UIKeyboardDidShowNotification
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWasHidden:)
												 name:UIKeyboardDidHideNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWasHidden:)
												 name:UIKeyboardWillHideNotification
											   object:nil];
	
	if ([self.dictWalker objectForKey:PARAM_NAME]) {
		[self.lblDriverName setText:[NSString stringWithFormat:@"Driver : %@",[self.dictWalker objectForKey:PARAM_NAME]]];
	}
	else{
		[self.lblDriverName setText:[NSString stringWithFormat:@"Driver : %@",[self.dictWalker objectForKey:PARAM_FIRST_NAME]]];
	}
}

- (void)setLocalizedStrings {
    [self.btnSend setTitle:NSLocalizedString(@"SEND", nil) forState:UIControlStateNormal];
    [self.message setPlaceholder:NSLocalizedString(@"SAY_SOMETHING_NICE", nil)];
}

- (void)layoutSetup {
    tableHeight = tableForChat.frame.size.height;
    viewPosition = viewForMessage.frame.origin.y;
    
    [self.driverImgView applyRoundedCornersFull];
    [self.driverImgView downloadFromURL:[self.dictWalker objectForKey:PARAM_PICTURE] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //btnSend clipping
    _btnSend.layer.cornerRadius = 5;
    _btnSend.clipsToBounds = YES;
    
    //blurView
    _blurView.hidden = YES;
    //_blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
    //UITapGestureRecognizer to hide overlay view
    UITapGestureRecognizer *overlayTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideOverlayView:)];
    [self.blurView addGestureRecognizer:overlayTapGestureRecognizer];
}

-(void)viewWillAppear:(BOOL)animated
{
    is_first=YES;
    is_first_load=YES;
    tablePosition=0;
    [self GetChatHistory];
    timerForChat=[NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(GetChatHistory) userInfo:nil repeats:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [timerForChat invalidate];
    timerForChat=nil;
}

/*-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int y=0;
    if (textField==self.message)
    {
        y=240;
    }
    [self.scrView setContentOffset:CGPointMake(0, y) animated:YES];
    
}*/

#pragma mark -
#pragma mark - UIButton Action Methods
- (void)sendButtonPressed:(id)sender
{
  NSString *destination = [self.dictWalker objectForKey:@"email"];
  
    
    NSData *data = [self.message.text dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

  if ([destination length] == 0 || [text length] == 0) {
    return;
  }
  [self pushToDriver:text];
  [self.message setText:@""];

}

- (IBAction)enlargeImage:(id)sender {
    
    [_driverImgView applyRoundedCornersFull];
    //[_profileImgView setBackgroundColor:[UIColor redColor]];
    
    [UIView animateWithDuration:1 animations:^{
        
        // Animate it to double the size
        const CGFloat scale = 1;
        [_driverImgView setTransform:CGAffineTransformMakeScale(scale, scale)];
        
        _driverImgView.frame =CGRectMake((_mainView.frame.size.width/2)-(_driverImgView.frame.size.width/2),200,150,150);
        CGPoint centerImageView = _driverImgView.center;
        centerImageView.x = self.view.center.x;
        _driverImgView.center = centerImageView;
        _blurView.hidden = NO;
        
    }];
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    
    
    [_blurView.layer addAnimation:transition forKey:nil];
    //[self.overlayDriver.layer addAnimation:transition forKey:nil];
    //[self.lblPassengerName.layer addAnimation:transition forKey:nil];
    //_overlayDriver.hidden = NO;
    //_lblPassengerName.hidden = NO;
}

- (IBAction)onClickBack:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)hideOverlayView:(UITapGestureRecognizer *)tapGestureRecognizer {
    //[_profileImgView setBackgroundColor:[UIColor redColor]];
    //[_driverImgView.layer applyRoundedCornersFullWithColor:[UIColor colorWithRed:0.30 green:0.71 blue:0.67 alpha:1.0]];
    
    [UIView animateWithDuration:1 animations:^{
        
        // Animate it to double the size
        const CGFloat scale = 1;
        [_driverImgView setTransform:CGAffineTransformMakeScale(scale, scale)];
        
        _driverImgView.frame = CGRectMake(12,20,35,35);
        [_driverImgView applyRoundedCornersFull];
        _blurView.hidden = YES;
    }];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [_blurView.layer addAnimation:transition forKey:nil];
    
    //[self.lblPassengerName.layer addAnimation:transition forKey:nil];
    
    //_lblPassengerName.hidden = YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{/*
    int y=0;
    
    if (textField==self.message)
    {
        y=240;
    }
    
    NSLog(@"TableForChat : %lu", (unsigned long)[_messages count]);
    //    NSLog(@"Y : %d", y);
    
    [self.tableForChat setFrame:CGRectMake(0, y, 320, 220)];
    
    [self.scrView setContentOffset:CGPointMake(0, y) animated:YES];*/
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self.tableForChat setFrame:CGRectMake(0, 0, 320, 445)];
    [self.scrView setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
	CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
	[viewForMessage setFrame:CGRectMake(viewForMessage.frame.origin.x,self.view.frame.size.height - keyboardSize.height - viewForMessage.frame.size.height, viewForMessage.frame.size.width, viewForMessage.frame.size.height)];
	[self scrollToBottom];
//
//    [self.tableForChat setFrame:CGRectMake(0, 0, 320,  self.scrView.frame.size.height - keyboardSize.height)];
}

- (void)keyboardWasHidden:(NSNotification *)notification
{
	[viewForMessage setFrame:CGRectMake(viewForMessage.frame.origin.x, viewPosition, viewForMessage.frame.size.width, viewForMessage.frame.size.height)];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==self.message){
        [textField resignFirstResponder];
        [self.scrView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
//    [textField resignFirstResponder];
    return YES;
}

#pragma mark - 
#pragma mark - WS Methods

-(void)pushToDriver:(NSString*)message
{
	if ([APPDELEGATE connected])
    {
		NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
		
		[dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
		[dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
		[dictParam setObject:[self.dictWalker objectForKey:PARAM_EMAIL] forKey:@"provider_email"];
		[dictParam setObject:message forKey:@"message"];
		
		AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		[helper getDataFromPath:FILE_SEND_PUSH_TO_WALKER withParamData:dictParam withBlock:^(id response, NSError *error) {
			if (response)
            {
                if([[response valueForKey:@"success"] boolValue])
                {
                    [_messages removeAllObjects];
                    [_messages addObjectsFromArray:[response valueForKey:@"message"]];
                    [self makeSection];
                    [self scrollToBottom];
                }
                else
                {
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                }
			}
		}];
	}
}

-(void)GetChatHistory
{
    if ([APPDELEGATE connected])
    {
        if(is_first_load==YES)
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        }
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[self.dictWalker objectForKey:PARAM_EMAIL] forKey:@"provider_email"];
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_CHAT_HISTORY withParamData:dictParam withBlock:^(id response, NSError *error) {
            if (response)
            {
                [USERDEFAULT setObject:[response objectForKey:@"unread"] forKey:PREF_CHAT_COUNTER];
                [USERDEFAULT synchronize];
                is_first_load=NO;
                [APPDELEGATE hideLoadingView];
                NSMutableArray *arr=[response valueForKey:@"message"];
                if(arr.count!=_messages.count)
                {
                    [_messages removeAllObjects];
                    [_messages addObjectsFromArray:[response valueForKey:@"message"]];
                    [self makeSection];
                    
                    if(is_first==YES)
                    {
                        is_first=NO;
                        [self scrollToBottom];
                    }
                    else
                    {
                        if(tablePosition==self.tableForChat.contentOffset.y)
                        {
                            [self scrollToBottom];
                        }
                    } 
                    
                }
                if(tablePosition<self.tableForChat.contentOffset.y)
                {
                    tablePosition=self.tableForChat.contentOffset.y;
                }
            }
        }];
    }
}

#pragma mark -
#pragma mark - sudtom setting methods
- (void)scrollToBottom
{
	if ([arrForSection count] > 0)
    {
        NSMutableArray *entryData=[arrForSection objectAtIndex:(arrForSection.count-1)];
		NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(entryData.count - 1)inSection:(arrForSection.count-1)];
        [self.tableForChat scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:UITableViewRowAnimationTop];
    }
}

#pragma mark-
#pragma mark - Table view data source

-(void)makeSection
{
    NSMutableArray *arrTemp=[[NSMutableArray alloc] init];
    [arrTemp addObjectsFromArray:_messages];
    for (int s=0; s<[arrTemp count]; s++) {
        
        NSMutableDictionary *dictTrip = [[arrTemp objectAtIndex:s] mutableCopy];
        dictTrip =  [super cleanNullInJsonDic:dictTrip];
        [arrTemp replaceObjectAtIndex:s withObject:dictTrip];
    }
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrTemp];
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"sent_at" ascending:YES selector:@selector(localizedStandardCompare:)];
    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
    
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[arrtemp objectAtIndex:i];
        
        NSString *temp=[dictDate valueForKey:@"sent_at"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"sent_at"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
            }
        }
    }
    [self.tableForChat reloadData];
    [self scrollToBottom];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrForSection.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [[arrForSection objectAtIndex:section] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    headerView.backgroundColor=[[UIColor alloc] initWithRed:239.0/255.0f green:239.0/255.0f blue:244.0/255.0f alpha:1];
    
    UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
    lblDate.font=[UberStyleGuide fontRegular];
    lblDate.textAlignment=NSTextAlignmentCenter;
    lblDate.textColor=[UIColor darkGrayColor];
    NSString *strDate=[arrForDate objectAtIndex:section];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
    
    ///   YesterDay Date Calulation
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday withFormate:@"yyyy-MM-dd"];
    
    
    if([strDate isEqualToString:current])
    {
        lblDate.text=@"Today";
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        lblDate.text=@"Yesterday";
    }
    else
    {
        NSDate *date=[[UtilityClass sharedObject]stringToDate:strDate withFormate:@"yyyy-MM-dd"];
        NSString *text=[[UtilityClass sharedObject]DateToString:date withFormate:@"dd MMMM yyyy"];//2nd Jan 2015
        lblDate.text=text;
    }
    
    [headerView addSubview:lblDate];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *entry=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    NSString *emojiEscaped = [entry valueForKey:@"message"];
    NSData *emojiData = [emojiEscaped dataUsingEncoding:NSUTF8StringEncoding];
    NSString *emojiString = [[NSString alloc] initWithData:emojiData encoding:NSNonLossyASCIIStringEncoding];
    
    
    CGSize constraintO= CGSizeMake(250, MAXFLOAT);
    
    NSStringDrawingContext *contextO = [[NSStringDrawingContext alloc] init];
    CGSize boundingBoxO = [emojiString boundingRectWithSize:constraintO options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Avenir Book" size:16.0]}context:contextO].size;
    
    CGSize labelSizeO = CGSizeMake(ceil(boundingBoxO.width) , ceil(boundingBoxO.height));
    
    return labelSizeO.height+25;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *entry=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    MessageTableViewCell *cell = [self dequeOrLoadMessageTableViewCell:[[entry valueForKey:@"my_msg"] intValue]];
    
    NSString *emojiEscaped = [entry valueForKey:@"message"];
    NSData *emojiData = [emojiEscaped dataUsingEncoding:NSUTF8StringEncoding];
    NSString *emojiString = [[NSString alloc] initWithData:emojiData encoding:NSNonLossyASCIIStringEncoding];
    cell.lblMessage.text =emojiString;
    
    NSDate *dateOfTime=[[UtilityClass sharedObject]stringToDate:[entry valueForKey:@"sent_at"]];
    cell.lblTime.text=[[UtilityClass sharedObject]DateToString:dateOfTime withFormate:@"hh:mm aa"];
    
    [cell.bubbleView.layer setCornerRadius:15.0];
    
    return cell;
}

- (MessageTableViewCell *)dequeOrLoadMessageTableViewCell:(MessageDirection)direction
{
    NSString *identifier =
    [NSString stringWithFormat:@"%@MessageCell", (Incoming == direction) ? @"Incoming" : @"Outgoing"];
    MessageTableViewCell *cell = [self.tableForChat dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil][0];
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    
    if (direction == Incoming) {
        [cell.bubbleView setBackgroundColor:[UIColor lightGrayColor]];
        [cell.lblMessage setTextAlignment:NSTextAlignmentLeft];
        [cell.lblMessage setTextColor:[UIColor whiteColor]];
        cell.lblMessage.numberOfLines=0;
        [cell.bubbleImage applyRoundedCornersFull];
        [cell.bubbleImage downloadFromURL:[self.dictWalker objectForKey:PARAM_PICTURE] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
    }else{
        [cell.bubbleView setBackgroundColor:[UberStyleGuide colorDefault]];
        [cell.lblMessage setTextAlignment:NSTextAlignmentLeft];
        [cell.lblMessage setTextColor:[UIColor whiteColor]];
        cell.lblMessage.numberOfLines=0;
    }
    return cell;
}

@end
