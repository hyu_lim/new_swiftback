//
//  UserListVC.h
//  SwiftBack
//
//  Created by Elluminati on 25/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@interface UserListVC : BaseVC<UITableViewDataSource,UITableViewDelegate>
{
	__weak IBOutlet UIButton *menuBtn;
	
	
	__weak IBOutlet UITableView *tableForUsers;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (IBAction)onClickBack:(id)sender;

@end
