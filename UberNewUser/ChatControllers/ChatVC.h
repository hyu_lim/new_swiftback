

#import "BaseVC.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ChatVC : BaseVC<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>{
	
	float tableHeight,viewPosition;
}

@property (strong,nonatomic) NSDictionary *dictWalker;

@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UITextField *message;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *enlargeImage;

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *viewForMessage;

@property (weak, nonatomic) IBOutlet UIImageView *driverImgView;
@property (weak, nonatomic) IBOutlet UITableView *tableForChat;
@property (weak, nonatomic) IBOutlet UIScrollView *scrView;

- (IBAction)sendButtonPressed:(id)sender;
- (IBAction)onClickBack:(id)sender;
- (void)hideOverlayView:(UITapGestureRecognizer *)tapGestureRecognizer;

//@property (weak, nonatomic) IBOutlet UIScrollView *scrView;

@end
