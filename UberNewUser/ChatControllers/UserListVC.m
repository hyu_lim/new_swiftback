//
//  UserListVC.m
//  SwiftBack
//
//  Created by Elluminati on 25/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "UserListVC.h"
#import "UserCell.h"
#import "UIImageView+Download.h"
#import "ChatVC.h"
#import "SWRevealViewController.h"

@interface UserListVC ()

@end

@implementation UserListVC

- (void)viewDidLoad {
	[super viewDidLoad];
	[self customSetup];
    [self layoutSetup];
    [self setLocalizedStrings];
}

- (void)setLocalizedStrings {
    [self.lblNavTitle setText:NSLocalizedString(@"SELECT_DRIVER", nil)];
}

- (void)layoutSetup {
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getAssociatedWalkers];
}

-(void)customSetup{
	SWRevealViewController *revealViewController = self.revealViewController;
	if ( revealViewController )
	{
		[menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
		//Swipe to reveal menu
		[self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
	}
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)getAssociatedWalkers{
	
	if ([APPDELEGATE connected]) {
		[APPDELEGATE showLoadingWithTitle:@"Loading"];
		NSMutableDictionary *dictParam = [[NSMutableDictionary alloc] init];
		[dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
		[dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
		
		AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		[helper getDataFromPath:FILE_GET_WALKERS withParamData:dictParam withBlock:^(id response, NSError *error) {
			[APPDELEGATE hideLoadingView];
			if (response) {
                
                NSLog(@"RESPONSE : %@", response);
				
				if ([[response valueForKey:@"success"] boolValue])
                {
					arrWalkers = [response valueForKey:@"walkers"];
                    [tableForUsers reloadData];
				}
				else{
					if ([response valueForKey:@"error"]) {
						
						if ([[response valueForKey:@"error"] isEqualToString:@"Not a valid token"] || [[response valueForKey:@"error"] isEqualToString:@"Error occurred , Please login again"]) {
							
							[super logoutAlert];
							// [USERDEFAULT setBool:NO forKey:PREF_IS_LOGIN];
							// [USERDEFAULT removeObjectForKey:PREF_LOGIN_OBJECT];
							// [USERDEFAULT synchronize];
							//  [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
							
						}
					}
					// [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
				}
			}
			else{
				
			}
		}];
	}
	else{
		UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
		[alert show];
	}
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	// Get the new view controller using [segue destinationViewController].
	// Pass the selected object to the new view controller.
	
	if ([[segue identifier] isEqualToString:SEGUE_TO_CHAT]) {
		
		ChatVC *vc = [segue destinationViewController];
		[vc setDictWalker:sender];
	}
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	
	return  [arrWalkers count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	// unread
	UserCell *cell = [tableForUsers dequeueReusableCellWithIdentifier:@"Cell"];
	
	NSDictionary *dictWalker = [arrWalkers objectAtIndex:indexPath.row];
    if([[dictWalker valueForKey:@"unread"] intValue]==0)
    {
        cell.lblMessageCounter.hidden=YES;
    }
    else
    {
        cell.lblMessageCounter.hidden=NO;
        cell.lblMessageCounter.layer.masksToBounds = YES;
        cell.lblMessageCounter.layer.cornerRadius = (cell.lblMessageCounter.frame.size.width / 2);
        [cell.lblMessageCounter setText:[NSString stringWithFormat:@"%@",[dictWalker valueForKey:@"unread"]]];
        [cell.lblMessageCounter setBackgroundColor:[UIColor colorWithRed:255.0/255.0f green:82.0/255.0f blue:82.0/255.0f alpha:1]];
    }
	
	[cell.lblDriverName setText:[dictWalker objectForKey:PARAM_NAME]];
	[cell.driverProfileView downloadFromURL:[dictWalker objectForKey:PARAM_PICTURE] withPlaceholder:[UIImage imageNamed:@"PROPIC4"]];
	
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	NSDictionary *dictWalker = [arrWalkers objectAtIndex:indexPath.row];
	[self performSegueWithIdentifier:SEGUE_TO_CHAT sender:dictWalker];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 70.0f;
}

- (IBAction)onClickBack:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
}

@end
