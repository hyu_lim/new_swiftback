//
//  FacebookUtility.m
//  NewFBapiDemo
//
//  Created by Jignesh on 15/05/13.
//  Copyright (c) 2013 Jignesh. All rights reserved.
//

#import "FacebookUtility.h"

NSString *const UD_FBACCESSTOKENDATA=@"FBAccessTokenDataDictionary";

@implementation FacebookUtility

@synthesize delegate;
@synthesize dictUserInfo=_dictUserInfo,arrFBFriendList=_arrFBFriendList;


#pragma mark -
#pragma mark - Init And Shared Object

-(id) init
{
    if((self = [super init]))
    {
  
    }
    return self;
}



    // Do any additional setup after loading the view, typically from a nib.

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}
- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error
{
    NSLog(@"%@",result);
    if ([FBSDKAccessToken currentAccessToken]) {
        
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:@"me"
                                      parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
                                      HTTPMethod:@"GET"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            NSLog(@"%@",result);
            // Handle the result
        }];
        
        
        
        
        //        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
        //         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result2, NSError *error) {
        //
        //             if (!error) {
        //                 NSLog(@"fetched user:%@", result2);
        //                 NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [result2 valueForKey:@"id"]];
        //             }
        //         }];
    }
    
}
-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString *domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    
    [FBSDKAccessToken setCurrentAccessToken:Nil];
}


- (IBAction)onClickLogin:(id)sender {
}


@end
