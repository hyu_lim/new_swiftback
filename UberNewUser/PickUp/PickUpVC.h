//
//  PickUpVC.h
//  UberNewUser
//
//  Developed by Elluminati on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <NYSegmentedControl/NYSegmentedControl.h>


@interface PickUpVC : BaseVC<MKMapViewDelegate,CLLocationManagerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate,GMSMapViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate>
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSDictionary* aPlacemark;
    NSMutableArray *placeMarkArr;
    NSMutableArray *arrPaymentTypes;
    NSString *strFutureDate;
    NSString *strDistance;
    NSString *placeID;
    NSArray *routes;
	NSInteger time;
    NSInteger DistanceETACount;
    BOOL isList,changeCameraPosition,isQuickBlox;
    GMSMutablePath *pathpoliline;
}

@property CGRect positionForPassengerView;
@property UIActionSheet *paymentSheet;
@property (nonatomic, strong) NYSegmentedControl *customSegmentedControl;

@property (weak, nonatomic) IBOutlet UILabel *lblFemaleOnlyText;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentRequestTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceText;
@property (weak, nonatomic) IBOutlet UILabel *lblNotesText;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmedViewTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSeatRequestedTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblShareOnFBTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNotesTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentText;
@property (weak, nonatomic) IBOutlet UILabel *lblNavigation;
@property (weak, nonatomic) IBOutlet UILabel *distanceBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblFareAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblReferralMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblETA;
@property (weak, nonatomic) IBOutlet UILabel *lblSize;
@property (weak, nonatomic) IBOutlet UILabel *lblFare;
@property (weak, nonatomic) IBOutlet UILabel *lMinFare;
@property (weak, nonatomic) IBOutlet UILabel *lETA;
@property (weak, nonatomic) IBOutlet UILabel *lMaxSize;
@property (weak, nonatomic) IBOutlet UILabel *lSelectPayment;
@property (weak, nonatomic) IBOutlet UILabel *lRefralMsg;
@property (weak, nonatomic) IBOutlet UILabel *lRate_basePrice;
@property (weak, nonatomic) IBOutlet UILabel *lRate_distancecost;
@property (weak, nonatomic) IBOutlet UILabel *lRate_TimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblRate_BasePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblRate_DistancePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblRate_TimePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblRateCradNote;
@property (weak, nonatomic) IBOutlet UILabel *lblCarType;
@property (weak, nonatomic) IBOutlet UILabel *lblCarNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driverName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driverRate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driver_Carname;
@property (weak, nonatomic) IBOutlet UILabel *lbl_driver_CarNumber;
@property (weak, nonatomic) IBOutlet UILabel *female_only;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceTo;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceSeat;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceNote;
@property (weak, nonatomic) IBOutlet UILabel *lblPreferences;
@property (weak, nonatomic) IBOutlet UILabel *femaleOnlyLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblNumOfPassenger;

@property (weak, nonatomic) IBOutlet UITextField *txtPreferral;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtDestination;
@property (weak, nonatomic) IBOutlet UITextField *txtNotes;

@property (weak, nonatomic) IBOutlet UIButton *btnDeptDateTime;
@property (weak, nonatomic) IBOutlet UIButton *btnDateNext;
@property (weak, nonatomic) IBOutlet UIButton *btnNoOfPassengerTitle;
@property (weak, nonatomic) IBOutlet UIButton *female_only_switch;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnMyLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnETA;
@property (weak, nonatomic) IBOutlet UIButton *btnSelService;
@property (weak, nonatomic) IBOutlet UIButton *btnPickMeUp;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnCash;
@property (weak, nonatomic) IBOutlet UIButton *btnCard;
@property (weak, nonatomic) IBOutlet UIButton *btnRatecard;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnRideNow;
@property (weak, nonatomic) IBOutlet UIButton *paymentNotAddedNotice;
@property (weak, nonatomic) IBOutlet UIButton *paymodeModeBtn;
@property (weak, nonatomic) IBOutlet UIButton *additionalPassengerBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnFare;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *btnPayCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnPayRequest;
@property (weak, nonatomic) IBOutlet UIButton *bReferralSubmit;
@property (weak, nonatomic) IBOutlet UIButton *bReferralSkip;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (nonatomic) IBOutlet UIButton* revealButtonItem;

@property (weak, nonatomic) IBOutlet UIView *dateBackground;
@property (weak, nonatomic) IBOutlet UIView *infoBackground;
@property (weak, nonatomic) IBOutlet UIView *viewGoogleMap;
@property (weak, nonatomic) IBOutlet UIView *viewForCancel;
@property (weak, nonatomic) IBOutlet UIView *viewForPreferral;
@property (weak, nonatomic) IBOutlet UIView *viewForFareAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForReferralError;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *viewForMarker;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIView *paymentView;
@property (weak, nonatomic) IBOutlet UIView *viewETA;
@property (weak, nonatomic) IBOutlet UIView *viewForRateCard;
@property (weak, nonatomic) IBOutlet UIView *viewForConfirmRequestTwo;
@property (weak, nonatomic) IBOutlet UIView *blurViewForRequest;
@property (weak, nonatomic) IBOutlet UIView *blurViewForAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForDriver;
@property (weak, nonatomic) IBOutlet UIView *viewForAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForAcceptRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForDatePick;
@property (weak, nonatomic) IBOutlet UIView *viewForGreenOverlay;
@property (weak, nonatomic) IBOutlet UIView *viewForConfirmRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForAdditionalPassenger;
@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundCancel;
@property (weak, nonatomic) IBOutlet UIImageView *img_driver_profile;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIImageView *advanceBackground;

@property (weak, nonatomic) IBOutlet UITableView *tableForCity;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSTimer *timer;
@property (strong,nonatomic) NSTimer *timerForCheckReqStatus;
@property (strong,nonatomic) NSTimer *timerForRequestInProgress;

@property (nonatomic,weak) IBOutlet MKMapView *map;

@property (weak, nonatomic) IBOutlet UISwitch *femaleSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *FacebookShareSwitch;
@property (weak, nonatomic) IBOutlet UIDatePicker *RLDatePicker;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;

- (IBAction)ETABtnPressed:(id)sender;
- (IBAction)cashBtnPressed:(id)sender;
- (IBAction)cardBtnPressed:(id)sender;
- (IBAction)requestBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)btnSkipReferral:(id)sender;
- (IBAction)btnAddReferral:(id)sender;
- (IBAction)eastimateFareBtnPressed:(id)sender;
- (IBAction)closeETABtnPressed:(id)sender;
- (IBAction)RateCardBtnPressed:(id)sender;
- (IBAction)addSubtractPassengers:(id)sender;
- (IBAction)goBackToDateFromAddtionalPassenger:(id)sender;
- (IBAction)DestinationSearching:(id)sender;
- (IBAction)pickMeUpBtnPressed:(id)sender;
- (IBAction)cancelReqBtnPressed:(id)sender;
- (IBAction)myLocationPressed:(id)sender;
- (IBAction)selectServiceBtnPressed:(id)sender;

- (IBAction)onClickPaymentMode:(id)sender;
- (IBAction)onClickConfirmPassengers:(id)sender;
- (IBAction)onClickSwitch:(id)sender;
- (IBAction)onClickRideLater:(id)sender;
- (IBAction)onClickDateSelected:(id)sender;
- (IBAction)onClickGoHideDateView:(id)sender;
- (IBAction)onClickCancelAdvanceRide:(id)sender;
- (IBAction)onClickAdvanceEdit:(id)sender;
- (IBAction)onClickAdvanceConfirm:(id)sender;
- (IBAction)onClickFacebookShare:(id)sender;
- (IBAction)onClickChatBtn:(id)sender;

- (void)goToSetting:(NSString *)str;
- (void)goToRide;
- (void)rideNext;


@end
