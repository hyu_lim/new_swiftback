

#import "BaseVC.h"
#import  <GoogleMaps/GoogleMaps.h>
#import "SWRevealViewController.h"
#import "Constants.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import  <MapKit/MapKit.h>
#import "RatingBar.h"
#import "RateView.h"

@interface AdvancePickupVC : BaseVC<GMSMapViewDelegate,UIAlertViewDelegate, UIScrollViewDelegate>
{
    GMSMapView *mapView_;
    
    NSArray *routes;
	NSDictionary *dictWalker;
    NSMutableArray *arrReviewList;
    NSDictionary *dictReview;
	
}
@property (weak, nonatomic) IBOutlet UILabel *lblDateText;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeText;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceText;
@property (weak, nonatomic) IBOutlet UILabel *lblNote_Label;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfPassengerText;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentByText;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleModelText;
@property (weak, nonatomic) IBOutlet UILabel *lblGenderText;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupationText;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleColorText;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleNumberText;
@property (weak, nonatomic) IBOutlet UILabel *lblMutualFrdsText;
@property (weak, nonatomic) IBOutlet UILabel *lblBioText;

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnOpenInMaps;



@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong,nonatomic) NSDictionary *dictRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForGoogleMap;
@property (weak, nonatomic) IBOutlet UIView *frontView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;

@property (weak, nonatomic) IBOutlet UIImageView *UserImgView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfRate;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblPassengerCount;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleName;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleColor;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleNo;
@property (weak, nonatomic) IBOutlet UIImageView *paymentImgView;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@property (weak, nonatomic) IBOutlet UILabel *verticalLine3;

@property (weak, nonatomic) IBOutlet UILabel *lblNote;

@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIView *roundedRectView;
@property (weak, nonatomic) IBOutlet UIImageView *triangleImgView;
@property (weak, nonatomic) IBOutlet UILabel *lblBtnsBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;

@property (weak, nonatomic) IBOutlet UIView *viewForPlace;
@property (weak, nonatomic) IBOutlet UIView *viewForDoc;
@property (weak, nonatomic) IBOutlet UIView *viewForPerson;
@property (weak, nonatomic) IBOutlet UIView *viewForVehicleInfo;
@property (weak, nonatomic) IBOutlet UIView *viewForReview;

@property (weak, nonatomic) IBOutlet UIButton *btnPlaceIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnDocIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnVehicleIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnPersonIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnReviewIcon;

//Chat

@property (weak, nonatomic) IBOutlet UILabel *lblBio;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupation;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;

@property (weak, nonatomic) IBOutlet UILabel *vehicleNumber;

//view for showing review list
@property (strong, nonatomic) IBOutlet UIScrollView *horizontalScrollView;
@property (strong, nonatomic) IBOutlet UIView *baseView;
@property (strong, nonatomic) IBOutlet UIImageView *clientImgView;
@property (strong, nonatomic) IBOutlet UIImageView *placeholderImgView;
@property (strong, nonatomic) IBOutlet UILabel *lblClientName;
@property (strong, nonatomic) IBOutlet UILabel *lblClientComment;
@property (strong, nonatomic) IBOutlet RatingBar *clientRatingView;
@property (strong, nonatomic) IBOutlet UIButton *btnLeft;
@property (strong, nonatomic) IBOutlet UIButton *btnRight;
@property (weak, nonatomic) IBOutlet UIImageView *enlargeUserImgView;
@property (weak, nonatomic) IBOutlet UIView *overlayBlurView;

@property (strong,nonatomic) NSString *isOffer;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickAccept:(id)sender;
- (IBAction)onClickReject:(id)sender;
- (IBAction)toMaps:(id)sender;
- (IBAction)onClickChat:(id)sender;
- (IBAction)onClickPlaceIcon:(id)sender;
- (IBAction)onClickDocIcon:(id)sender;
- (IBAction)onClickVehicleIcon:(id)sender;
- (IBAction)onClickPersonIcon:(id)sender;
- (IBAction)onClickReviewIcon:(id)sender;
- (IBAction)enlargeImage:(id)sender;

- (void)slideUpWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;
- (void)slideDownWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;
- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer;
- (void)hideOverlayView:(UITapGestureRecognizer *)tapGestureRecognizer;

@end
