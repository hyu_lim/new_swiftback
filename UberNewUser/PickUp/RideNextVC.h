//
//  RideNextVC.h
//  SwiftBack
//
//  Created by My Mac on 10/15/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "SWRevealViewController.h"
#import "RatingBar.h"
#import "BaseVC.h"
#import <NYSegmentedControl/NYSegmentedControl.h>

@interface RideNextVC : BaseVC<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate,SWTableViewCellDelegate>
{

    NSMutableArray *arrRequests;
    
    __weak IBOutlet UITableView *tblRideNext;
    __weak IBOutlet UIView *viewForNavigation;
    __weak IBOutlet UIImageView *no_Items;

    __weak IBOutlet UIButton *menuBtn;
    __weak IBOutlet UIButton *btnRequest;
    __weak IBOutlet UISegmentedControl *requestSegment;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentText;

@property (weak, nonatomic) IBOutlet UILabel *lblFoundDriverText;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleNoText;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleColorText;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleModelText;



@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedToggle;
@property (strong, nonatomic) IBOutlet UIView *mainView;

//Green Overlay View
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIImageView *dottedLineImgView;

@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImgView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImgView;
@property (weak, nonatomic) IBOutlet UILabel *driverName;

@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (weak, nonatomic) IBOutlet RatingBar *confirmedRatingView;

@property (weak, nonatomic) IBOutlet UILabel *lblVehicleNo;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleColor;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleName;

@property (nonatomic, strong) NYSegmentedControl *customSegmentedControl;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

- (IBAction)onClickNewRequest:(id)sender;
- (IBAction)onClickSegment:(id)sender;

@end
