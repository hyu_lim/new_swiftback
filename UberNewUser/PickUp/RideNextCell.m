//
//  RideNextCell.m
//  SwiftBack
//
//  Created by My Mac on 10/15/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "RideNextCell.h"

@implementation RideNextCell

- (void)awakeFromNib {
	
	[self.ratingView initRateBar];
    [self setLocalizedStrings];
    // Initialization code
}

- (void)setLocalizedStrings {
    [self.lblDepartureDateText setText:NSLocalizedString(@"DEPARTURE_DATE", nil)];
    [self.lblDepartureTimeText setText:NSLocalizedString(@"DEPARTURE_TIME", nil)];
    [self.lblDistanceText setText:NSLocalizedString(@"DISTANCE", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
