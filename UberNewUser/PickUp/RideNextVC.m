//
//  RideNextVC.m
//  SwiftBack
//
//  Created by My Mac on 10/15/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "RideNextVC.h"
#import "RideNextCell.h"
#import "RideNextDriverCell.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "SWRevealViewController.h"
#import "ChatVC.h"

@interface RideNextVC ()
{
    UITableViewController *tableViewController;
    NSString *currencySign;
    NSString *currentCountry;
}
@end

@implementation RideNextVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customSetup];
    [self layoutSetup];
    [self setLocalizedStrings];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideOverlayView:)];
    [self.overlayView addGestureRecognizer:tapRecognizer];
//    self.overlayView.tag=2;
    
//    //Pull to refresh table view configuration
//    tableViewController = [[UITableViewController alloc] init];
//    tableViewController.tableView = tblRideNext;
//    
//    self.refreshControl = [[UIRefreshControl alloc] init];
//    self.refreshControl.backgroundColor = [UIColor clearColor];
//    self.refreshControl.tintColor = [UIColor darkGrayColor];
//    [self.refreshControl addTarget:self action:@selector(callRequestMethod) forControlEvents:UIControlEventValueChanged];
//    tableViewController.refreshControl = self.refreshControl;
}

- (void)setLocalizedStrings {
    [self.lblNavTitle setText:NSLocalizedString(@"ADVANCE_RIDES", nil)];
    [self.lblPaymentText setText:NSLocalizedString(@"PAYMENT_TEXT", nil)];
    [self.lblFoundDriverText setText:NSLocalizedString(@"WE_FOUND_YOU_A_DRIVER", nil)];
    [self.lblVehicleNoText setText:NSLocalizedString(@"VEHICLE_NO", nil)];
    [self.lblVehicleColorText setText:NSLocalizedString(@"VEHICLE_COLOR", nil)];
    [self.lblVehicleModelText setText:NSLocalizedString(@"VEHICLE_MODEL", nil)];
}

//- (void)callRequestMethod {
//    
//    if(self.customSegmentedControl.selectedSegmentIndex == 0) {
//        [self getRequests:@"0"];
//    } else if(self.customSegmentedControl.selectedSegmentIndex == 1) {
//        [self getRequests:@"1"];
//    }
//}

- (void)viewWillAppear:(BOOL)animated {
    [self getRequests:@"0"];
}

- (void)layoutSetup {
    
    [no_Items setHidden:YES];
    //    UIPanGestureRecognizer *swipeGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(cellSwipe:)];
    //    [swipeGesture setMinimumNumberOfTouches:1];
    //    [swipeGesture setDelegate:self];
    //    [tblRideNext addGestureRecognizer:swipeGesture];
    
    //Segmented button properties
    CGRect frame= _segmentedToggle.frame;
    [_segmentedToggle setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 25)];
    
    //register button
    btnRequest.layer.cornerRadius = 15;
    btnRequest.layer.borderWidth = 1;
    btnRequest.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 0.5;
    _backgroundRect.layer.borderColor = [UIColor colorWithRed:59.f/255.f green:177.f/255.f blue:156.f/255.f alpha:1].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //blurView
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.blurView.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.blurView addSubview:blurEffectView];
    }
    else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
    //NYSegmentedControl
    self.customSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[@"Pending", @"Confirmed"]];
    self.customSegmentedControl.frame = CGRectMake(10, 60, self.view.frame.size.width - 20, 35);
    
    [self.customSegmentedControl addTarget:self action:@selector(segmentSelected) forControlEvents:UIControlEventValueChanged];
    
    self.customSegmentedControl.titleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleFont = [UIFont systemFontOfSize:12.0f];
    
    self.customSegmentedControl.backgroundColor = [UIColor colorWithRed:37.f/255.f green:131.f/255.f blue:111.f/255.f alpha:1.0f];
    
    self.customSegmentedControl.borderWidth = 0.0f;
    self.customSegmentedControl.segmentIndicatorBackgroundColor = [UIColor colorWithRed:59.f/255.f green:177.f/255.f blue:156.f/255.f alpha:1.0f];
    self.customSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    self.customSegmentedControl.segmentIndicatorInset = 2.0f;
    self.customSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    
    self.customSegmentedControl.cornerRadius = CGRectGetHeight(self.customSegmentedControl.frame) / 2.0f;
    
    [self.view addSubview:self.customSegmentedControl];
    
    self.customSegmentedControl.contentMode = UIViewContentModeScaleAspectFill;
    
    self.customSegmentedControl.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin);
    
    self.customSegmentedControl.clipsToBounds = YES;
    
    
    [self.driverImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.vehicleImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    
    //ratingBar
    [self.ratingView initRateBar];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)getRequests:(NSString*)requestType
{
    NSLog(@"RequestType : %@", requestType);
    
    if ([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Loading"];
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
		[dictParam setObject:requestType forKey:PARAM_CONFIRM];

		
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_GET_FUTURE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error) {
            
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                if ([[response objectForKey:@"success"] boolValue])
                {
					
                    if([response objectForKey:@"currency_sign"]) {
                        currencySign = [response objectForKey:@"currency_sign"];
                        NSLog(@"Sign : %@", currencySign);
                    } else {
                        currencySign = @"$";
                    }
                    
                    if([response objectForKey:@"current_country"]) {
                        currentCountry = [response objectForKey:@"current_country"];
                        NSLog(@"CC : %@", currentCountry);
                    } else {
                        currentCountry = @"Singapore";
                    }
                    
                    arrRequests = [[response valueForKey:@"request"] mutableCopy];
					
                    if ([arrRequests count]>0)
                    {
                        [tblRideNext reloadData];
                        [tblRideNext setHidden:NO];
                        [no_Items setHidden:YES];
//						[self.dottedLineImgView setHidden: NO];

                    }
                    else
                    {
//                        [self.dottedLineImgView setHidden: YES];
                        [tblRideNext setHidden:YES];
                        [no_Items setHidden:NO];
                    }
                }
                else
                {
					if ([[response objectForKey:@"error"]isEqualToString:@"Error occurred , Please login again"] || [[response objectForKey:@"error"] isEqualToString:@"Not a valid token"]) {
		
						[super logoutAlert];
					}
                    [arrRequests removeAllObjects];
                    [no_Items setHidden:NO];
					[self.dottedLineImgView setHidden: YES];

                    [tblRideNext reloadData];
                    [tblRideNext setHidden:YES];
                }
                
            }
            
//            [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }];
    }
}

//- (void)reloadData {
//    
//    //Reload table data
//    [tblRideNext reloadData];
//    
//    //End the refreshing
//    if(self.refreshControl) {
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"MMM d, h:mm a"];
//        NSString *title = [NSString stringWithFormat:@"Last update : %@", [formatter stringFromDate:[NSDate date]]];
//        NSDictionary *attrDictionary = [NSDictionary dictionaryWithObject:[UIColor darkGrayColor] forKey:NSForegroundColorAttributeName];
//        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrDictionary];
//        self.refreshControl.attributedTitle = attributedTitle;
//        [self.refreshControl endRefreshing];
//    }
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrRequests.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *const identifier1 = @"cell";
    NSString *const identifier2 = @"cell1";
    
    //cell1 properties
    RideNextCell *cell1 = [tableView dequeueReusableCellWithIdentifier:identifier1];
    UIView *cell1BackgroundView = [cell1 viewWithTag:900];
    cell1BackgroundView.backgroundColor = [UIColor whiteColor];
    
    cell1BackgroundView.layer.cornerRadius = 10;
    cell1BackgroundView.layer.shadowRadius = 5.0;
    cell1BackgroundView.layer.shadowOpacity = 0.4;
    cell1BackgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell1BackgroundView.layer.borderWidth = 0.5;
    
    cell1BackgroundView.clipsToBounds = YES;
    
    cell1.backgroundColor = [UIColor clearColor];
    cell1.layer.backgroundColor = [UIColor clearColor].CGColor;
    
    //Adjust horizontal and vertical lines size
    UILabel *horizontalLine = [cell1 viewWithTag:903];
    UILabel *verticalLine1 = [cell1 viewWithTag:904];
    UILabel *verticalLine2 = [cell1 viewWithTag:905];
    
    horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
    
    verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
    
    verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);
    
    //cell2 properties
    
    RideNextDriverCell *cell2 = [tableView dequeueReusableCellWithIdentifier:identifier2];
    
    //New View in Cell2
    UIView *cell2BackgroundView = [cell2 viewWithTag:901];
    cell2BackgroundView.backgroundColor = [UIColor whiteColor];
    
    cell2BackgroundView.layer.cornerRadius = 10;
    cell2BackgroundView.clipsToBounds = YES;
    
    cell2.backgroundColor = [UIColor clearColor];
    cell2.layer.backgroundColor = [UIColor clearColor].CGColor;
    
    //viewForDriverInfo
    UIView *viewForDriverInfo = [cell2 viewWithTag:1000];
    viewForDriverInfo.layer.cornerRadius = 10;
    
    UIImageView *imgViewForDriverInfo = [cell2 viewWithTag:1001];
    imgViewForDriverInfo.layer.cornerRadius = 10;
    
    [cell2.confirmedRatingView initRateBar];
    [cell2.confirmedRatingView setUserInteractionEnabled: NO];

    
    NSDictionary *dictRequest = [arrRequests objectAtIndex:indexPath.row];
    
    NSLog(@"DICTREQUEST : %@", dictRequest);
    
    if ([[dictRequest valueForKey:@"walker"]count] > 0)
    {
        
        
        NSDictionary *dictWalker = [dictRequest valueForKey:@"walker"];
        NSArray *seperatedName = [[dictWalker valueForKey:@"name"] componentsSeparatedByString:@" "];
        
        [cell2.lblDriverName setText:[NSString stringWithFormat:@"%@",[seperatedName objectAtIndex:0]]];
        [cell2.lblDriverName2 setText:[NSString stringWithFormat:@"%@",[seperatedName objectAtIndex:0]]];

        
        RBRatings ratings = ([[dictWalker objectForKey:@"rating"] floatValue] * 2);
        [cell2.confirmedRatingView setRatings:ratings];
        
        
        [cell2.lblVehicleColor setText:[dictWalker valueForKey:@"car_color"]];
        [cell2.lblVehicleMake setText:[dictWalker valueForKey:@"car_type"]];
        [cell2.lblVehicleNumber setText:[dictWalker valueForKey:@"car_number"]];
        [cell2.driverImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
        [cell2.driverImgView downloadFromURL:[dictWalker valueForKey:@"picture"] withPlaceholder:nil];
        [cell2.vehicleImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
        [cell2.vehicleImgView downloadFromURL:[dictWalker valueForKey:@"vehicle_picture"] withPlaceholder:nil];
        [cell2.lblFromStation setText:[dictRequest valueForKey:@"s_address"]];
        [cell2.lblToStation setText:[dictRequest valueForKey:@"d_address"]];
    
        
        [cell2.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:@"total"]]];
        [cell2.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
        
        NSDate *date = [[UtilityClass sharedObject] stringToDate:[dictRequest valueForKey:@"future_request_date"] withFormate:@"yyyy-MM-dd"];
        
        [cell2.lblDepartureDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
        [cell2.lblDepartureTime setText:[dictRequest valueForKey:@"future_request_time"]];
		[cell2.chatBtn setTag:indexPath.row];
		[cell2.chatBtn addTarget:self action:@selector(goToChat:) forControlEvents:UIControlEventTouchUpInside];
		
        
        // Add utility buttons
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        
        
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.898 green:0.451 blue:0.451 alpha:1]
                                                    title:@"Cancel"];
        
        cell2.rightUtilityButtons = rightUtilityButtons;
        cell2.delegate = self;

        
        // Configure the cell...
      //  cell2.patternImageView.image = [UIImage imageNamed:@"pin"];
        
        return cell2;
    }
    else
    {
        
        
        [cell1.lblFromStation setText:[dictRequest valueForKey:@"s_address"]];
        [cell1.lblToStation setText:[dictRequest valueForKey:@"d_address"]];
        
        [cell1.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:@"total"]]];
        [cell1.lblDistance setText:[NSString stringWithFormat:@"%0.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
                    
        NSDate *date = [[UtilityClass sharedObject] stringToDate:[dictRequest valueForKey:@"future_request_date"] withFormate:@"yyyy-MM-dd"];
        
        [cell1.lblDepartureDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
        [cell1.lblDepartureTime setText:[dictRequest valueForKey:@"future_request_time"]];
        
        // Add utility buttons
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:0.898 green:0.451 blue:0.451 alpha:1]
                                                    title:@"X"];
        
        cell1.rightUtilityButtons = rightUtilityButtons;
        cell1.delegate = self;
        
        return cell1;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
    NSDictionary *dictRequest = [arrRequests objectAtIndex:indexPath.row];
    
//    if ([[dictRequest valueForKey:@"walker"]count] > 0 && self.segmentedToggle.selectedSegmentIndex == 1)
    if ([[dictRequest valueForKey:@"walker"]count] > 0 && self.customSegmentedControl.selectedSegmentIndex == 1)
    {
        
         [self.customSegmentedControl setHidden:YES];
        
        NSDictionary *dictWalker = [dictRequest valueForKey:@"walker"];
        NSArray *seperatedName = [[dictWalker valueForKey:@"name"] componentsSeparatedByString:@" "];
        
        [self.driverName setText:[NSString stringWithFormat:@"%@",[seperatedName objectAtIndex:0]]];
//        [cell2.lblDriverName2 setText:[NSString stringWithFormat:@"%@",[seperatedName objectAtIndex:0]]];
        
        
        [self.lblVehicleColor setText:[dictWalker valueForKey:@"car_color"]];
        [self.lblVehicleName setText:[dictWalker valueForKey:@"car_type"]];
        [self.lblVehicleNo setText:[dictWalker valueForKey:@"car_number"]];
        [self.driverImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
        [self.driverImgView downloadFromURL:[dictWalker valueForKey:@"picture"] withPlaceholder:nil];
        [self.vehicleImgView downloadFromURL:[dictWalker valueForKey:@"vehicle_picture"] withPlaceholder:nil];

//        [self.ratingView initRateBar];
        [self.ratingView setUserInteractionEnabled:NO];
		RBRatings ratings = (float)([[dictWalker objectForKey:PARAM_RATING] floatValue] * 2);
		[self.ratingView setRatings:ratings];
        [self.blurView setHidden: NO];
        [self.overlayView setHidden: NO];
    }
}

- (void)hideOverlayView : (UITapGestureRecognizer *)sender {
    [self.blurView setHidden:YES];
    [self.overlayView setHidden: YES];
    [self.customSegmentedControl setHidden: NO];
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
              case 0:
        {
            UIAlertController *cancelRequestAlert = [UIAlertController alertControllerWithTitle:@"Cancel Ride Request" message:@"Are you sure to cancel ride request?" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                // Delete button is pressed
                NSIndexPath *cellIndexPath = [tblRideNext indexPathForCell:cell];
                //            [tblRideNext beginUpdates];
                //            [tblRideNext deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
                
                NSString *reqID = [[arrRequests objectAtIndex:cellIndexPath.row] objectForKey:@"request_id"];
                [self cancelRequest:reqID indexPath:cellIndexPath];
                //  [arrRequests removeObjectAtIndex:cellIndexPath.row];
                //  [tblRideNext endUpdates];
                [self canBecomeFirstResponder];
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            [cancelRequestAlert addAction:okAction];
            [cancelRequestAlert addAction:cancelAction];
            [self presentViewController:cancelRequestAlert animated:YES completion:nil];
            
            break;
        }
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictRequest = [arrRequests objectAtIndex:indexPath.row];
    
    if ([[dictRequest valueForKey:@"walker"]count] > 0)
    {
//        return 235.0f;
        return 250.0f;
    }
    else
    {
//        return 120.0f;
//        return 127.0f;
        return 105.0f;
    }
}

-(IBAction)goToChat:(UIButton*)sender{
	
	CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tblRideNext];
	NSIndexPath *indexPath = [tblRideNext indexPathForRowAtPoint:buttonPosition];

	NSDictionary *dictWalker = [[arrRequests objectAtIndex:indexPath.row] objectForKey:@"walker"];
	
	[self performSegueWithIdentifier:SEGUE_TO_DIRECT_CHAT sender:dictWalker];
	
	
}

- (void)cancelRequest:(NSString*)reqID indexPath:(NSIndexPath*)indexpath
{
        if([APPDELEGATE connected])
        {
            [APPDELEGATE  showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setValue:reqID forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [APPDELEGATE hideLoadingView];

                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         //[APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         [tblRideNext beginUpdates];
                         [tblRideNext deleteRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationLeft];
                           [arrRequests removeObjectAtIndex:indexpath.row];
                           [tblRideNext endUpdates];
                         
                         if ([arrRequests count] == 0)
                         {
                             [tblRideNext setHidden:YES];
                             [no_Items setHidden:NO];
                         }
                     }
                     else
                     {}
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
}

- (IBAction)onClickNewRequest:(id)sender
{
    [USERDEFAULT setBool:YES forKey:@"resetFutureReq"];
    [USERDEFAULT synchronize];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)onClickSegment:(id)sender
{
    
    if ([requestSegment selectedSegmentIndex]==0)
    {
        [self getRequests:@"0"];
        [self.dottedLineImgView setHidden: YES];
    }
    else
    {
        [self getRequests:@"1"];
        [self.dottedLineImgView setHidden: NO];
    }
}

- (void)segmentSelected {
    
    if ([self.customSegmentedControl selectedSegmentIndex]==0)
    {
        [self getRequests:@"0"];
        [self.dottedLineImgView setHidden: YES];
    }
    else
    {
        [self getRequests:@"1"];
        [self.dottedLineImgView setHidden: NO];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:SEGUE_TO_DIRECT_CHAT]) {
		ChatVC *vc = [segue destinationViewController];
		vc.dictWalker = sender;
	}
}

@end
