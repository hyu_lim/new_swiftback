//
//  ExpandingCellTableViewCell.m
//  SwiftBack
//
//  Created by Hengyu Lim on 11/17/15.
//  Copyright (c) 2015 Swiftback. All rights reserved.
//

#import "ExpandingCellTableViewCell.h"

@implementation ExpandingCellTableViewCell
@synthesize ltextLabel, fruitLabel, titleLabel, calcLabel, calculationLabel, subtitleLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
